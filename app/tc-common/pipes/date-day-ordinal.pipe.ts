import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "sgAppDateDayOrdinal"
})

export class DateDayOrdinal implements PipeTransform {

    transform(displayDate: Date): string {
        const day = displayDate.getUTCDate();
        let dispWithOrd: string;

        switch (day) {
            case 1:
            case 21:
            case 31:
                dispWithOrd = day + "st";
                break;
            case 2:
            case 22:
                dispWithOrd = day + "nd";
                break;
            case 3:
            case 23:
                dispWithOrd = day + "rd";
                break;
            default:
                dispWithOrd = day + "th";
                break;
        }

        return dispWithOrd;
    }
}
