import { Component, Input } from "@angular/core";
import { DatePicker } from "tns-core-modules/ui/date-picker";
import { PickerService } from "~/services/picker.service";

@Component ({
    selector: "app-date-picker-cell",
    moduleId: module.id,
    templateUrl: "./date-picker-cell.component.html"
})

export class DatePickerCellComponent {
    @Input() date: Date;

    loading: boolean = true;

    constructor(
        private pickerService: PickerService
    ) {
        console.log("constructor()");
    }

    onPickerLoaded(args) {
        const datePicker: DatePicker = <DatePicker>args.object;
        datePicker.date = this.date;
    }

    onDateChange(args): void {
        if (!this.loading) {
            this.pickerService.handleNewDateSelected(args.value);
        } else {
            this.loading = false;
        }
    }
}
