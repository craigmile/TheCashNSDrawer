import { Component, Input, OnInit } from "@angular/core";

import { EventData } from "tns-core-modules/data/observable";
import { TextField } from "tns-core-modules/ui/text-field";

// services
import { CellService } from "~/services/cell.service";
// interfaces
import { IListCell, ListCellType } from "../interfaces/IListCell.interface";

@Component ({
    selector: "app-editable-cell",
    moduleId: module.id,
    templateUrl: "./editable-cell.component.html"
})

export class EditableCellComponent implements OnInit {
    @Input() cellInfo: IListCell;

    textField: TextField;
    isExpense: boolean = false;
    kbdType: string;

    constructor(
        private cellService: CellService
    ) {
        // listen for a focus event (next button on keyboard from a previous editable cell)
        this.cellService.focusOnInlineEditableCellWithIndex.subscribe((idx) => {
            if (this.cellInfo.index === idx) {
                console.log("focus on me: " + idx);
                this.textField.focus();
            }
        });
    }

    ngOnInit() {
        switch (this.cellInfo.cellType) {
            case ListCellType.monetaryAmount:
                this.kbdType = "number";
                const valueString = this.cellInfo.value.replace("$", "");
                const valueNumeric: number = parseFloat(valueString);
                if (!isNaN(valueNumeric)) {
                    // console.log("Monetary display cell with value: " + valueNumeric);
                    if (valueNumeric > 0) {
                        this.isExpense = true;
                    }
                }
                break;
            default:
                this.kbdType = "url";
                break;
        }
    }

    //
    // Actions
    //

    onTextChange(args: any): void {
        // console.log("onTextChange()");
        const textField = <TextField>args.object;
        // send an output event - to remain loosely coupled
        this.cellInfo.value = textField.text;
        this.cellService.editableCellValueChange.emit(this.cellInfo);
    }

    // for clear button
    onTextFieldLoaded(args: EventData): void {
        // console.log("onTextFieldLoaded()");
        this.textField = <TextField>args.object;
        // note: args.ios is an instance of UITextField
        if (this.textField.ios) {
            this.textField.ios.clearButtonMode = UITextFieldViewMode.WhileEditing;
        }
    }

    // for clear button padding
    onTextFieldFocus(args: EventData): void {
        // console.log("onTextFieldFocus()");
        const txtFld: TextField = <TextField>args.object;
        if (txtFld.ios) {
            txtFld.paddingRight = 30;
        }
    }

    // for clear button padding
    onTextFieldBlur(args: EventData): void {
        // console.log("onTextFieldBlur()");
        const txtFld: TextField = <TextField>args.object;
        if (txtFld.ios) {
            txtFld.paddingRight = 10;
        }
    }

    // let the enclosing component handle it - next editable cell
    onReturnPress(args: any): void {
        const textField = <TextField>args.object;
        console.log("onReturnPress(): text in cell: " + textField.text);
        // emit an event so that the form can focus on the next inline entry cell
        const nextCellIdx = this.cellService.getIndexForNextInlineEditableCell(this.cellInfo.index);
        console.log("onReturnPress(): next cell for focus: " + nextCellIdx);
        this.cellService.focusOnInlineEditableCellWithIndex.emit(nextCellIdx);
    }
}
