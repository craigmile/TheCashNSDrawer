import { Component, Input, OnChanges } from "@angular/core";

import { ListCellType } from "../interfaces/IListCell.interface";

@Component ({
    selector: "app-display-cell",
    moduleId: module.id,
    templateUrl: "./display-cell.component.html"
})

export class DisplayCellComponent implements OnChanges {
    @Input() description: string;
    @Input() value: string;
    @Input() cellType: ListCellType;

    isExpense: boolean = false;

    ngOnChanges(): void {
        this.isExpense = false;
        if (this.cellType === ListCellType.monetaryAmount) {
            const valueString = this.value.replace("$", "");
            const valueNumeric: number = parseFloat(valueString);
            if (!isNaN(valueNumeric)) {
                // console.log("Monetary display cell with value: " + valueNumeric);
                if (valueNumeric > 0) {
                    this.isExpense = true;
                }
            }
        }
    }
}
