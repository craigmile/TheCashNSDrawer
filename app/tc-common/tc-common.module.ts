import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { DatePickerCellComponent } from "~/tc-common/cells/date-picker-cell.component";
import { DisplayCellComponent } from "~/tc-common/cells/display-cell.component";
import { EditableCellComponent } from "~/tc-common/cells/editable-cell.component";
import { DateDayOrdinal } from "~/tc-common/pipes/date-day-ordinal.pipe";

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [
        DateDayOrdinal,
        DatePickerCellComponent,
        DisplayCellComponent,
        EditableCellComponent
    ],
    declarations: [
        DateDayOrdinal,
        DatePickerCellComponent,
        DisplayCellComponent,
        EditableCellComponent
    ],
    providers: [],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class TcCommonModule {}
