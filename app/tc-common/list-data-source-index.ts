import { isIOS } from "tns-core-modules/platform";

// tslint:disable-next-line:max-line-length
export const platformSpecificListDataSource = isIOS ?
    require("./list-data-source-ios").ListDataSource :
    require("./list-data-source-android").ListDataSource;
