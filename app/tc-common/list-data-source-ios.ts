/**
 * Modifies the data source for a UITableView
 * Add the following functionality to the NativeScript <ListView>:
 *  + swipe to delete
 *  + section headers (sticky headers)
 */
import { Subject } from "rxjs";

import { ListView } from "tns-core-modules/ui/list-view/list-view";
import { ISectionHeader } from "../transactions/transaction-list/section-header.interface";

export class ListDataSource extends NSObject implements UITableViewDataSource {

    // tslint:disable-next-line:variable-name
    static ObjCProtocols = [UITableViewDataSource];

    static initWithOwner(
        owner: WeakRef<ListView>,
        headersArrayiOS: Array<ISectionHeader>
        ): ListDataSource {
        const dataSource = new ListDataSource();
        dataSource.owner = owner;
        dataSource.oldiOSDataSource = owner.get().ios.dataSource;
        dataSource.rowDeletedEventStream = new Subject();
        dataSource.headersArrayiOS = headersArrayiOS;
        dataSource.cumulativeRowCountArray = dataSource.calculateCumulativeRowCounts(headersArrayiOS);

        return dataSource;
    }

    rowDeletedEventStream: Subject<number>;

    private owner: WeakRef<ListView>;
    private oldiOSDataSource: UITableViewDataSource;
    private headersArrayiOS: Array<ISectionHeader>;
    private cumulativeRowCountArray: Array<number>;

    // modified to get sticky headers
    numberOfSectionsInTableView?(tableView: UITableView): number {
        return this.headersArrayiOS.length;
    }

    // modified to get sticky headers
    tableViewTitleForHeaderInSection?(tableView: UITableView, section: number): string {
        return this.headersArrayiOS[section].title;
    }

    // modified to get sticky headers
    tableViewNumberOfRowsInSection(tableView: UITableView, section: number): number {
        return this.headersArrayiOS[section].numRows;
    }

    // pass through to NS ListView code
    tableViewCellForRowAtIndexPath(tableView: UITableView, indexPath: NSIndexPath): UITableViewCell {
        const idx: number = this.cumulativeRowCountArray[indexPath.section] + indexPath.row;
        // console.log("tableViewCellForRowAtIndexPath(): row:" + indexPath.row +
        // "; section: " + indexPath.section + "; idx: " + idx);
        const tmpIndexPath =  NSIndexPath.indexPathForItemInSection(idx, 0);

        return this.oldiOSDataSource.tableViewCellForRowAtIndexPath(tableView, tmpIndexPath);
    }

    // modified to cater for swipe delete
    tableViewCommitEditingStyleForRowAtIndexPath(
        tableView: UITableView,
        editingStyle: UITableViewCellEditingStyle,
        indexPath: NSIndexPath) {

        // in case NS implements this method in the future
        if (this.oldiOSDataSource.tableViewCommitEditingStyleForRowAtIndexPath !== undefined) {
            this.oldiOSDataSource.tableViewCommitEditingStyleForRowAtIndexPath(
                tableView, editingStyle, indexPath
            );
        } else if (editingStyle === UITableViewCellEditingStyle.Delete) {
            const owner: ListView = this.owner.get();
            if (owner) {
                const flatIdx: number = this.cumulativeRowCountArray[indexPath.section] + indexPath.row;
                // const items = owner.items as Array<any>;
                // items.splice(flatIdx, 1); // remove 1 item

                // convert row, section to a one dimensional array index
                // const flatIndexPath =  NSIndexPath.indexPathForItemInSection(flatIdx, 0);
                // const flatIdxPathArr: NSArray<NSIndexPath> = NSArray.arrayWithObject(flatIndexPath);
                // tableView.deleteRowsAtIndexPathsWithRowAnimation(
                //     flatIdxPathArr,
                //     UITableViewRowAnimation.Fade
                // );

                // tell the business logic to delete the item from the database and the UI
                this.rowDeletedEventStream.next(flatIdx);
            }
        }
    }

    private calculateCumulativeRowCounts(headersArrayiOS: Array<ISectionHeader>): Array<number> {
        const cumulativeRowCounts: Array<number> = [0];
        let cumulativeCount: number = 0;

        for (const hdr of headersArrayiOS) {
            cumulativeCount += hdr.numRows;
            cumulativeRowCounts.push(cumulativeCount);
        }

        return cumulativeRowCounts;
    }
}
