export interface ISubCategory {
    PK: number;
    majorCategoryPK: number;
    subCategoryName: string;
}
