export interface IMajorCategory {
    PK: number;
    transactionType: number;
    majorCategoryName: string;
}
