export enum ListCellType {
    monetaryAmount,
    hierarchyString,
    descriptionString,
    dateString,
    datePicker
}

export interface IListCell {
    index: number;
    cellType: ListCellType;
    editable: boolean;
    description?: string; // left side of cell
    value?: string; // right side of cell: may have no value (eg: date picker)
    PK?: number;
    date?: Date;
}
