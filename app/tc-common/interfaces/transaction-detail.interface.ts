export interface ITransactionDetail {
    PK: number;
    tranDate: Date;
    tranType: number;
    majorCategoryPK: number;
    majorCategoryName: string;
    subCategoryPK: number;
    subCategoryName: string;
    amount: number;
    tax: number;
    payMethodPK: number;
    payMethodName: string;
    accountPK: number;
    accountName: string;
    referenceID: string;
    notes: string;
}
