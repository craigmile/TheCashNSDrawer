export enum EditMode {
    create,
    read,
    update,
    delete
}
