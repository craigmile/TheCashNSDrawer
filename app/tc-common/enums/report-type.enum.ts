export enum ReportType {
    income = "Income",
    expense = "Expense"
}
