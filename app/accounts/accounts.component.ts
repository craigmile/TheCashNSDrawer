/**
 * Provide a list of available accounts
 */
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import * as app from "application";
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

import * as appSettings from "tns-core-modules/application-settings";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";

import { IAccount } from "~/accounts/account.interface";

import { AccountService } from "~/services/account.service";
import { SharedDataService } from "~/services/shared-data.service";

import { DisplayMode } from "~/tc-common/enums/display-mode.enum";
import { UserDefaultKeys } from "~/tc-common/enums/user-default-keys.enum";

@Component({
    selector: "accounts",
    moduleId: module.id,
    templateUrl: "./accounts.component.html"
})

export class AccountsComponent implements OnInit {
    uiListCells: Array<IAccount>;
    selectedIdx: number;
    displayMode: DisplayMode = DisplayMode.sideDrawer;

    constructor(
        private accountService: AccountService,
        private sharedDataService: SharedDataService,
        private route: ActivatedRoute,
        private routerExtensions: RouterExtensions
    ) {}

    ngOnInit(): void {
        this.route.queryParams.subscribe((params) => {
            if (params.displayMode !== undefined) {
                this.displayMode = JSON.parse(params.displayMode);
            }
        });

        this.accountService.getAccounts((accounts: Array<IAccount>, errorMsg?: string) => {
            // is there an account already selected?
            const selectedAcctPK = appSettings.getNumber(UserDefaultKeys.accountPrimaryKey);
            let i = 0;
            let account: IAccount;
            for (account of accounts) {
                if (account.PK === selectedAcctPK) {
                    this.selectedIdx = i;
                    break;
                }
                i++;
            }
            console.log("ngOnInit(): selected index = " + this.selectedIdx);

            this.uiListCells = accounts;
        });
    }

    //
    // Actions
    //

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args: ItemEventData): void {
        console.log("onItemTap(): idx=" + args.index);
        this.selectedIdx = args.index;
        const selectedAccountPK: number = this.uiListCells[args.index].PK;
        const selectedAccountName: string = this.uiListCells[args.index].accountName;

        if (this.sharedDataService.transactionDetail) {
            this.sharedDataService.popToTransactionDetailAndRefresh = true;
            this.sharedDataService.transactionDetail.accountPK = selectedAccountPK;
            this.sharedDataService.transactionDetail.accountName = selectedAccountName;
        }

        appSettings.setNumber(UserDefaultKeys.accountPrimaryKey, selectedAccountPK);
        appSettings.setString(UserDefaultKeys.accountName, selectedAccountName);

        if (this.displayMode === DisplayMode.hierarchy) {
            this.routerExtensions.backToPreviousPage();
        }
    }

    onAddPressed() {
        console.log("onAddPressed()");
    }
}
