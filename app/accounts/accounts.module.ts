import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { AccountsRoutingModule } from "~/accounts/accounts-routing.module";
import { AccountsComponent } from "~/accounts/accounts.component";
import { AccountService } from "~/services/account.service";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        AccountsRoutingModule
    ],
    declarations: [
        AccountsComponent
    ],
    providers: [
        AccountService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AccountsModule { }
