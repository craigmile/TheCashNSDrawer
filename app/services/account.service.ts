/**
 * CRUD for account/s on the local database.
 */
import { Injectable } from "@angular/core";

import { IAccount } from "~/accounts/account.interface";

const sqlite = require("nativescript-sqlite");
const databaseFileName = "TheCash.sqlite"; // TODO: move somewhere common

@Injectable()
export class AccountService {

    //
    // Uses a callback.
    //
    getAccounts(callback: (result: Array<IAccount>, errorMessage?: string) => void): void {
        if (!sqlite.exists(databaseFileName)) {
            sqlite.copyDatabase(databaseFileName);
        }

        // open a database connection... note: underlying plugin uses promises...
        new sqlite(databaseFileName).then(
            (dbConnection) => {
                const sqlStmt: string = `select Z_PK, ZACCOUNTNAME
                from ZACCOUNTDATAENTITY order by ZACCOUNTNAME`;
                dbConnection.all(sqlStmt).then(
                    (rows) => {
                        let newRow: IAccount;
                        const accounts = new Array<IAccount>();
                        for (const row of rows) {
                            newRow = { PK: row[0], accountName: row[1] };
                            accounts.push(newRow);
                        }
                        callback(accounts, null);
                    },
                    (error) => {
                        console.error(error, error.stack);
                        callback(null, error);
                    }
                );
            },
            (error) => {
                console.error(error, error.stack);
                callback(null, error);
            }
        );
    }
}
