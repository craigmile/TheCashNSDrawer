/**
 * CRUD for category/s on the local database.
 */
import { Injectable } from "@angular/core";

import { IMajorCategory } from "~/tc-common/interfaces/major-category.interface";

const sqlite = require("nativescript-sqlite");
const databaseFileName = "TheCash.sqlite";

@Injectable()
export class MajorCategoryService {
    getMajorCategories(callback: (result: Array<IMajorCategory>, errorMessage?: string) => void): void {
        if (!sqlite.exists(databaseFileName)) {
            sqlite.copyDatabase(databaseFileName);
        }

        // open a database connection... note: underlying plugin uses promises...
        new sqlite(databaseFileName).then(
            (dbConnection) => {
                const sqlStmt: string = `select Z_PK, ZTRANTYPE, ZMAJORCATEGORYNAME
                from ZMAJORCATEGORYDATAENTITY order by ZMAJORCATEGORYNAME`;
                dbConnection.all(sqlStmt).then(
                    (rows) => {
                        let newRow: IMajorCategory;
                        const majorCategories = new Array<IMajorCategory>();
                        for (const row of rows) {
                            newRow = { PK: row[0], transactionType: row[1], majorCategoryName: row[2] };
                            majorCategories.push(newRow);
                        }
                        callback(majorCategories, null);
                    },
                    (error) => {
                        console.error(error, error.stack);
                        callback(null, error);
                    }
                );
            },
            (error) => {
                console.error(error, error.stack);
                callback(null, error);
            }
        );
    }

    getMajorCategoriesForTransactionType(
        transactionType: number,
        callback: (result: Array<IMajorCategory>, errorMessage?: string) => void): void {

        if (!sqlite.exists(databaseFileName)) {
            sqlite.copyDatabase(databaseFileName);
        }

        // open a database connection... note: underlying plugin uses promises...
        new sqlite(databaseFileName).then(
            (dbConnection) => {
                const sqlStmt: string = `select Z_PK, ZTRANTYPE, ZMAJORCATEGORYNAME
                from ZMAJORCATEGORYDATAENTITY where ZTRANTYPE=` + transactionType + ` order by ZMAJORCATEGORYNAME`;
                dbConnection.all(sqlStmt).then(
                    (rows) => {
                        let newRow: IMajorCategory;
                        const majorCategories = new Array<IMajorCategory>();
                        for (const row of rows) {
                            newRow = { PK: row[0], transactionType: row[1], majorCategoryName: row[2] };
                            majorCategories.push(newRow);
                        }
                        console.log("getMajorCategoriesForTransactionType: success. Found " +
                        majorCategories.length + " categories");
                        callback(majorCategories, null);
                    },
                    (error) => {
                        console.error(error, error.stack);
                        callback(null, error);
                    }
                );
            },
            (error) => {
                console.error(error, error.stack);
                callback(null, error);
            }
        );
    }

    getMajorCategory(PK: number, callback: (result: IMajorCategory, errorMessage?: string) => void): void {
        if (!sqlite.exists(databaseFileName)) {
            sqlite.copyDatabase(databaseFileName);
        }

        // open a database connection... underlying plugin uses promises... limiting us
        new sqlite(databaseFileName).then(
            (dbConnection) => {
                const sqlStmt: string = `select Z_PK, ZTRANTYPE, ZMAJORCATEGORYNAME
                from ZMAJORCATEGORYDATAENTITY where Z_PK=` + PK;
                dbConnection.all(sqlStmt).then(
                    (rows) => {
                        const dbRow = rows[0];
                        callback(dbRow, null);
                    },
                    (error) => {
                        console.error(error, error.stack);
                        callback(null, error);
                    }
                );
            },
            (error) => {
                console.error(error, error.stack);
                callback(null, error);
            }
        );
    }
 }
