import { EventEmitter, Injectable, Output } from "@angular/core";
// import { IPaymentMethod } from "~/tc-common/interfaces/payment-method.interface";

@Injectable()
export class PickerService {

    @Output() newDateSelected = new EventEmitter<Date>(); // date from a date picker

    handleNewDateSelected(newDate: Date): void {
        console.log("handleNewDateSelected(" + newDate + ")");
        this.newDateSelected.emit(newDate);
    }
}
