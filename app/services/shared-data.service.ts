import { Injectable } from "@angular/core";

import { ReportType } from "~/tc-common/enums/report-type.enum";
import { ITransactionDetail } from "~/tc-common/interfaces/transaction-detail.interface";

@Injectable({
    providedIn: "root"
})
export class SharedDataService {
    transactionDetail: ITransactionDetail;
    popToTransactionDetailAndRefresh: boolean;
    reportType: ReportType;

    constructor() {
        this.transactionDetail = {
            PK: 0,
            tranDate: new Date(),
            tranType: 0,
            majorCategoryPK: 0,
            majorCategoryName: "",
            subCategoryPK: 0,
            subCategoryName: "",
            amount: 0,
            tax: 0,
            payMethodPK: 0,
            payMethodName: "",
            accountPK: 0,
            accountName: "",
            referenceID: "",
            notes: ""
          };

        this.popToTransactionDetailAndRefresh = false; // TODO: loosely couple this
        this.reportType = ReportType.income;
    }
}
