/**
 * CRUD for sub-category/s on the local database.
 */
import { Injectable } from "@angular/core";

import { ISubCategory } from "~/tc-common/interfaces/sub-category.interface";

const sqlite = require("nativescript-sqlite");
const databaseFileName = "TheCash.sqlite";

@Injectable()
export class SubCategoryService {
    //
    // Use a callback
    //
    getSubCategories(callback: (result: Array<ISubCategory>, errorMessage?: string) => void): void {
        if (!sqlite.exists(databaseFileName)) {
            sqlite.copyDatabase(databaseFileName);
        }

        // open a database connection... note: underlying plugin uses promises...
        new sqlite(databaseFileName).then(
            (dbConnection) => {
                const sqlStmt: string =
                `select Z_PK, ZMAJORCATEGORY, ZSUBCATEGORYNAME
                    from ZSUBCATEGORYDATAENTITY
                    order by ZSUBCATEGORYNAME`;

                dbConnection.all(sqlStmt).then(
                    (rows) => {
                        let newRow: ISubCategory;
                        const subCategories = new Array<ISubCategory>();
                        for (const row of rows) {
                            newRow = { PK: row[0], majorCategoryPK: row[1], subCategoryName: row[2] };
                            subCategories.push(newRow);
                        }
                        callback(subCategories, null);
                    },
                    (error) => {
                        console.error(error, error.stack);
                        callback(null, error);
                    }
                );
            },
            (error) => {
                console.error(error, error.stack);
                callback(null, error);
            }
        );
    }

    getSubCategoriesForMajorCategory(
        majorCategoryPK: number,
        callback: (result: Array<ISubCategory>, errorMessage?: string) => void): void {
        if (!sqlite.exists(databaseFileName)) {
            sqlite.copyDatabase(databaseFileName);
        }

        // open a database connection... underlying plugin uses promises... limiting us
        new sqlite(databaseFileName).then(
            (dbConnection) => {
                const sqlStmt: string =
                `select Z_PK, ZMAJORCATEGORY, ZSUBCATEGORYNAME
                    from ZSUBCATEGORYDATAENTITY
                    where ZMAJORCATEGORY=` + majorCategoryPK;

                dbConnection.all(sqlStmt).then(
                    (rows) => {
                        let newRow: ISubCategory;
                        const subCategories = new Array<ISubCategory>();
                        for (const row of rows) {
                            newRow = { PK: row[0], majorCategoryPK: row[1], subCategoryName: row[2] };
                            subCategories.push(newRow);
                        }
                        callback(subCategories, null);
                    },
                    (error) => {
                        console.error(error, error.stack);
                        callback(null, error);
                    }
                );
            },
            (error) => {
                console.error(error, error.stack);
                callback(null, error);
            }
        );
    }
 }
