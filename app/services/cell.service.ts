import { EventEmitter, Injectable, Output } from "@angular/core";

import { IListCell } from "~/tc-common//interfaces/IListCell.interface";

@Injectable()
export class CellService {
    @Output() editableCellValueChange = new EventEmitter<IListCell>();
    @Output() focusOnInlineEditableCellWithIndex = new EventEmitter<number>();

    inlineEditableCellIndexes: Array<number>;

    setInlineEditableCellIndexes(inlineEditableCellIndexes: Array<number>) {
        this.inlineEditableCellIndexes = inlineEditableCellIndexes;
    }

    getIndexForNextInlineEditableCell(currCellIdx: number): number {
        // console.log("getIndexForNextInlineEditableCell()");
        if (this.inlineEditableCellIndexes === undefined) {
            return;
        }
        let rqdIdx: number;
        let i: number = 0;
        let cellIdx: number;
        for (cellIdx of this.inlineEditableCellIndexes) {
            if (cellIdx === currCellIdx) {
                rqdIdx = i;
                break;
            }
            i++;
        }

        if (rqdIdx < this.inlineEditableCellIndexes.length - 1) {
            rqdIdx++;

            return this.inlineEditableCellIndexes[rqdIdx];
        } else {
            return undefined;
        }
    }
}
