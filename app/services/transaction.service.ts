/**
 * CRUD for transaction/s on the local database.
 */

import { Injectable } from "@angular/core";

import { ITransactionDetail } from "~/tc-common/interfaces/transaction-detail.interface";
import { ITransaction } from "~/transactions/transaction-list/transaction.interface";

const sqlite = require("nativescript-sqlite");
const databaseFileName = "TheCash.sqlite";

@Injectable()
export class TransactionService {

  createTransaction(
    newTransaction: ITransactionDetail,
    callback: (result: number, errorMessage?: string) => void) {
      // make sure that the database file is copied to the platform
      if (!sqlite.exists(databaseFileName)) {
          sqlite.copyDatabase(databaseFileName);
      }

      // open a database connection
      new sqlite(databaseFileName).then(
          (dbConnection) => {
              // getTime() gives the milliseconds since 1st Jan 1970 until the date
              // subtracting 978307200 seconds converts the number of seconds to seconds from 1st Jan 2001
              // until the date, which is what we want in the database
              let secondsForDatabase: number  = Math.floor(newTransaction.tranDate.getTime() / 1000);
              secondsForDatabase = secondsForDatabase - 978307200;
              const sqlStmt: string =
                `insert into ZTRANSACTIONDATAENTITY (
                  ZTRANPERCPERSONALUSE,
                  ZTRANTAXINCLUDEDINPRICE,
                  ZTRANTYPE,
                  ZTRANDATE,
                  ZACCOUNT,
                  ZMAJORCATEGORY,
                  ZPAYMENTMETHOD,
                  ZSUBCATEGORY,
                  ZTRANAMOUNT,
                  ZTRANTAX,
                  ZTRANCURR,
                  ZTRANNOTES,
                  ZTRANREFERENCEID )
                values (
                  100,
                  1,` +
                  newTransaction.tranType + "," +
                  secondsForDatabase + "," +
                  newTransaction.accountPK + "," +
                  newTransaction.majorCategoryPK + "," +
                  newTransaction.payMethodPK + "," +
                  newTransaction.subCategoryPK + "," +
                  newTransaction.amount + "," +
                  newTransaction.tax + "," +
                  "'AUD'," +
                  "'" + newTransaction.notes + "'," +
                  "'" + newTransaction.referenceID + "'" +
                ")";
              // console.log("createTransaction(): sql = " + sqlStmt);

              dbConnection.all(sqlStmt).then(
                () => {
                  callback(null, null);
                },
                (error) => {
                  console.error("ERROR: Creating new transaction");
                  console.error(error, error.stack);
                  callback(null, error);
                }
              );
          }
      );
  }

  //
  // Get all transactions
  //
  readTransactions(
    accountPK: number,
    callback: (result: Array<ITransaction>, errorMessage?: string) => void
  ): void {
    console.log("readTransactions()): for account " + accountPK);
    // make sure that the database file is copied to the platform
    if (!sqlite.exists(databaseFileName)) {
      sqlite.copyDatabase(databaseFileName);
    }

    const transactions = new Array<ITransaction>();
    let newRow: ITransaction;
    let rqdDate: Date;

    // open a database connection
    new sqlite(databaseFileName).then(
      (dbConnection) => {
        // We store dates as seconds after Jan 1st, 2001 rather than Jan 1st 1970.
        // 31 years accounting for leap years is 978307200 seconds.
        // Adding 978307200 seconds will move the date to the epoch.
        const sqlStmt: string =
          `select distinct
            a.Z_PK,
            strftime('%Y-%m-%d', datetime(ZTRANDATE + 978307200, 'unixepoch', 'localtime')) as tranDate,
            b.ZMAJORCATEGORYNAME,
            c.ZSUBCATEGORYNAME,
            a.ZTRANAMOUNT,
            a.ZTRANTYPE
          from
            ZTRANSACTIONDATAENTITY a,
            ZMAJORCATEGORYDATAENTITY b,
            ZSUBCATEGORYDATAENTITY c,
            ZACCOUNTDATAENTITY d
          where
            a.ZMAJORCATEGORY = b.Z_PK and
            a.ZSUBCATEGORY = c.Z_PK and
            a.ZACCOUNT = ` + accountPK + `
          order by tranDate desc, ZTRANAMOUNT desc`;

        dbConnection.all(sqlStmt).then(
          (rows) => {
            for (const row of rows) {
              rqdDate = new Date(row[1]);
              newRow = {
                PK: row[0],
                tranDate: rqdDate,
                majorCategoryName: row[2],
                subCategoryName: row[3],
                amount: row[4],
                type: row[5]
              };
              transactions.push(newRow);
            }
            console.log("TransactionService: readTransactions(): got " +
                transactions.length + " transactions on account " + accountPK);
            callback(transactions, null);
          },
          (error) => {
            console.error("ERROR: Selecting all transactions");
            console.error(error, error.stack);
            callback(null, error);
          }
        );
      },
      (error) => {
        console.error("ERROR: Opening database connection");
        console.error(error, error.stack);
        callback(null, error);
      }
    );
  }

  //
  // Get a particular transaction: based on it's primary key
  //
  readTransactionDetail(
    transactionPK: number,
    callback: (result: ITransactionDetail, errorMessage?: string) => void
  ): void {
    // make sure that the database file is copied to the platform
    if (!sqlite.exists(databaseFileName)) {
      sqlite.copyDatabase(databaseFileName);
    }

    // open a database connection... underlying plugin uses promises... limiting us
    new sqlite(databaseFileName).then(
      (dbConnection) => {
        const sqlStmt: string =
          `select
            a.Z_PK,
            strftime('%Y-%m-%d', datetime(ZTRANDATE + 978307200, 'unixepoch', 'localtime')) as tranDate,
            a.ZMAJORCATEGORY,
            b.ZMAJORCATEGORYNAME,
            a.ZSUBCATEGORY,
            c.ZSUBCATEGORYNAME,
            a.ZTRANTYPE,
            ZTRANAMOUNT,
            ZTRANTAX,
            a.ZPAYMENTMETHOD,
            d.ZMETHODNAME,
            a.ZACCOUNT,
            e.ZACCOUNTNAME,
            ZTRANREFERENCEID,
            ZTRANNOTES
          from
            ZTRANSACTIONDATAENTITY a,
            ZMAJORCATEGORYDATAENTITY b,
            ZSUBCATEGORYDATAENTITY c,
            ZPAYMENTMETHODDATAENTITY d,
            ZACCOUNTDATAENTITY e
          where
            a.ZMAJORCATEGORY = b.Z_PK and
            a.ZSUBCATEGORY = c.Z_PK and
            a.ZPAYMENTMETHOD = d.Z_PK and
            a.ZACCOUNT = e.Z_PK and
            a.Z_PK=` + transactionPK;

        console.log("readTransactionDetail(): sql: " + sqlStmt);

        dbConnection.all(sqlStmt).then(
          (rows) => {
            const dbRow = rows[0];
            const rqdDate = new Date(dbRow[1]);
            const tranDetail: ITransactionDetail = {
              PK: dbRow[0],
              tranDate: rqdDate,
              majorCategoryPK: dbRow[2],
              majorCategoryName: dbRow[3],
              subCategoryPK: dbRow[4],
              subCategoryName: dbRow[5],
              tranType: dbRow[6],
              amount: dbRow[7],
              tax: dbRow[8],
              payMethodPK: dbRow[9],
              payMethodName: dbRow[10],
              accountPK: dbRow[11],
              accountName: dbRow[12],
              referenceID: dbRow[13],
              notes: dbRow[14]
            };
            callback(tranDetail, null);
          },
          (error) => {
            console.error(error, error.stack);
            callback(null, error);
          }
        );
      },
      (error) => {
        console.error(error, error.stack);
        callback(null, error);
      }
    );
  }

  updateTransaction(transactionDetail: ITransactionDetail,
                    callback: (result: number, errorMessage?: string) => void) {
    // make sure that the database file is copied to the platform
    if (!sqlite.exists(databaseFileName)) {
        sqlite.copyDatabase(databaseFileName);
    }

    // getTime() gives the milliseconds since 1st Jan 1970 until the date
    // subtracting 978307200 seconds converts the number of seconds to seconds from 1st Jan 2001
    // until the date, which is what we want in the database
    // note: tranDate is a string "YYYY-mm-dd"
    // we want the number of seconds from 1st Jan 2001 to the date for the database
    let secondsForDatabase: number  = Math.floor(transactionDetail.tranDate.getTime() / 1000);
    secondsForDatabase = secondsForDatabase - 978307200;

    // TODO:   ZTRANPERCPERSONALUSE = ` + transactionDetail.accountName + `,` + `
    // TODO:   ZTRANTAXINCLUDEDINPRICE = ` + transactionDetail. + `,` + `

    // open a database connection... underlying plugin uses promises... limiting us
    new sqlite(databaseFileName).then(
      (dbConnection) => {
        const sqlStmt: string =
        `update ZTRANSACTIONDATAENTITY
          set
            ZTRANTYPE = ` + transactionDetail.tranType + `,` + `
            ZACCOUNT = ` + transactionDetail.accountPK + `,` + `
            ZMAJORCATEGORY = ` + transactionDetail.majorCategoryPK + `,` + `
            ZPAYMENTMETHOD = ` + transactionDetail.payMethodPK + `,` + `
            ZSUBCATEGORY = ` + transactionDetail.subCategoryPK + `,` + `
            ZTRANAMOUNT = ` + transactionDetail.amount + `,` + `
            ZTRANDATE = ` + secondsForDatabase + `,` + `
            ZTRANTAX = ` + transactionDetail.tax + `,` + `
            ZTRANNOTES = '` + transactionDetail.notes + `',` + `
            ZTRANREFERENCEID = '` + transactionDetail.referenceID + `'
          where
            Z_PK=` + transactionDetail.PK;

        console.log("updateTransaction(): sql: " + sqlStmt);

        dbConnection.all(sqlStmt).then(
          () => {
            callback(null, null);
          },
          (error) => {
            console.error("ERROR: updateTransaction(): Creating new transaction");
            console.error(error, error.stack);
            callback(null, error);
          }
        );
      },
      (error) => {
        console.error("ERROR: updateTransaction(): Opening database connection");
        console.error(error, error.stack);
        callback(null, error);
      });
  }

  //
  // Delete transaction for a specified primary key
  //
  deleteTransaction(
    transactionPK: number,
    callback: (result: any, errorMessage?: string) => void
  ): void {
    // make sure that the database file is copied to the platform
    if (!sqlite.exists(databaseFileName)) {
      sqlite.copyDatabase(databaseFileName);
    }

    const sqlStmt: string = `delete from ZTRANSACTIONDATAENTITY where Z_PK=` + transactionPK;
    console.log("deleteTransaction(): sql: " + sqlStmt);

    // open a database connection
    new sqlite(databaseFileName).then((dbConnection) => {
      dbConnection.all(sqlStmt).then(
        () => {
          callback(null, null);
        },
        (error) => {
          console.error("ERROR: deleteTransaction(): could not delete transaction");
          console.error(error, error.stack);
          callback(null, error);
        }
      );
    },
    (error) => {
      console.error("ERROR: deleteTransaction(): opening database connection");
      console.error(error, error.stack);
      callback(null, error);
    }
   );
  }

}
