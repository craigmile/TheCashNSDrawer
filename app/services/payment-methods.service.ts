/**
 * CRUD for payment methods on the local database.
 */
import { Injectable } from "@angular/core";

import { IPaymentMethod } from "~/tc-common/interfaces/payment-method.interface";

const sqlite = require("nativescript-sqlite");
const databaseFileName = "TheCash.sqlite";

@Injectable()
export class PaymentMethodsService {
    getPaymentMethods(callback: (result: Array<IPaymentMethod>, errorMessage?: string) => void): void {
        if (!sqlite.exists(databaseFileName)) {
            sqlite.copyDatabase(databaseFileName);
        }

        // open a database connection... note: underlying plugin uses promises...
        new sqlite(databaseFileName).then(
            (dbConnection) => {
                const sqlStmt: string = `select Z_PK, ZMETHODNAME
                from ZPAYMENTMETHODDATAENTITY order by ZMETHODNAME`;
                dbConnection.all(sqlStmt).then(
                    (rows) => {
                        let newRow: IPaymentMethod;
                        const paymentMethods = new Array<IPaymentMethod>();
                        for (const row of rows) {
                            newRow = { PK: row[0], methodName: row[1] };
                            paymentMethods.push(newRow);
                        }
                        // console.log("getPaymentMethods(): success: found: " + paymentMethods.length);
                        callback(paymentMethods, null);
                    },
                    (error) => {
                        console.error(error, error.stack);
                        callback(null, error);
                    }
                );
            },
            (error) => {
                console.error(error, error.stack);
                callback(null, error);
            }
        );
    }
 }
