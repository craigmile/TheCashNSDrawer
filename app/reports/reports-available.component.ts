/**
 * Types of reports. eg:
 *  income
 *  expenses
 */
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { RouterExtensions } from "nativescript-angular/router";

import { SharedDataService } from "~/services/shared-data.service";

import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";
import { ReportType } from "~/tc-common/enums/report-type.enum";

@Component({
    selector: "reports-available",
    moduleId: module.id,
    templateUrl: "./reports-available.component.html"
})

export class ReportsAvailableComponent implements OnInit {
    uiListCells: Array<string>;
    reportName: string;
    reportType: ReportType;

    constructor(
        private route: ActivatedRoute,
        private routerExtensions: RouterExtensions,
        private sharedDataService: SharedDataService
    ) {
        this.route.queryParams.subscribe((params) => {
            this.reportType = params.selectedReportType;
            console.log("selected report type: " + this.reportType);
            this.reportName = this.reportType as string;
            console.log("selected report name: " + this.reportName);
        });
    }

    ngOnInit(): void {
        this.uiListCells = Object.keys(ReportType).map((k) => ReportType[k as string]); // eg: ["Expenses", "Income"];
    }

    //
    // Actions
    //

    onItemTap(args: ItemEventData): void {
        // this.selected = this.uiListCells[args.index];
        console.log("onItemTap(): idx: " + args.index);
        this.sharedDataService.reportType = ReportType[args.index] as ReportType;
        switch (args.index) {
            case 0:
                this.sharedDataService.reportType = ReportType.income;
                break;
            default:
                this.sharedDataService.reportType = ReportType.expense;
                break;
        }
        console.log("onItemTap(): report type: " + this.sharedDataService.reportType);
        this.routerExtensions.backToPreviousPage();
    }
}
