import { Injectable } from "@angular/core";

import * as appSettings from "tns-core-modules/application-settings";
import { File, Folder, knownFolders } from "tns-core-modules/file-system";

import { Observable, of } from "rxjs";

import { MajorCategoryService } from "~/services/major-category.service";
import { TransactionType } from "~/tc-common/enums/transaction-type.enum";
import { IMajorCategory } from "~/tc-common/interfaces/major-category.interface";

@Injectable()
export class ReportGeneratorService {
    majorCategoriesArr: Array<IMajorCategory>;

    constructor(private majorCategoryService: MajorCategoryService) {}

    //
    // Create the report as a csv file in the Documents directory
    //
    async generateReport(accountName: string, transactionType: number, startDate: Date,
                         endDate: Date): Promise<string> {
        console.log("generateReport(): " + accountName + "; transactionType: " + transactionType +
        "; startDate: " + startDate.toDateString() + "; endDate: " + endDate.toDateString());

        // async/await for the heading
        const heading = await this.csvHeadingLineForReportType(transactionType);
        console.log("generateReport(): got a heading: " + heading);

        const documentsFolder: Folder = <Folder>knownFolders.documents();
        // create a new file
        let reportFileName: string = accountName.replace(/\W/g, "");
        if (transactionType === TransactionType.income) {
            reportFileName += "_Income_";
        } else {
            reportFileName += "_Expense_";
        }

        let yyyy: string = String(startDate.getFullYear());
        let mm: string = String(startDate.getMonth() + 1);
        let dd: string = String(startDate.getDate());
        reportFileName += yyyy;
        reportFileName += "_" + mm;
        reportFileName += "_" + dd + "_to";

        yyyy = String(endDate.getFullYear());
        mm = String(endDate.getMonth() + 1);
        dd = String(endDate.getDate());
        reportFileName += "_" + yyyy;
        reportFileName += "_" + mm;
        reportFileName += "_" + dd + ".csv";
        console.log("generateReport(): report file name = " + reportFileName);

        const reportFile: File = documentsFolder.getFile(reportFileName);
        const reportFilePath: string = reportFile.path;

        const promise: Promise<string> = new Promise((resolve, reject) => {
            reportFile.writeText(heading)
            .then(() => {
                reportFile.readText()
                    .then((res) => {
                        console.log("generateReport(): file write success");
                        resolve(reportFilePath);
                    });
            }).catch((err) => {
                console.log("generateReport():" + err);
                reject();
            });
        });

        return promise;
    }

    //
    // Date     Details     Total(incl Tax)     Tax     Total(excl Tax)    ... heading for each major category ...
    //
    async csvHeadingLineForReportType(transactionType: number): Promise<string> {
        console.log("csvHeadingLineForReportType()");

        let csvHeadingLine: string = await new Promise<string>((resolve, reject) => {
            // initial columns ...will be followed by major categories
            csvHeadingLine = "Date,Details,Total(incl Tax),Tax,Total(excl Tax)";

            // major categories for a transaction type
            this.majorCategoryService.getMajorCategoriesForTransactionType(
                transactionType,
                (majorCategories: Array<IMajorCategory>, errorMsg?: string) => { // callback for SQLite
                this.majorCategoriesArr = majorCategories;

                if (errorMsg) {
                    console.error("ERROR: could not get major categories for transaction type: " + transactionType);
                    reject();
                } else {
                    for (const majorCat of majorCategories) {
                        csvHeadingLine += "," + majorCat.majorCategoryName;
                    }
                }

                csvHeadingLine += "\n";

                resolve(csvHeadingLine);
            });
        });

        return csvHeadingLine;
    }

    //
    // Date  Details  Total(incl Tax)   Tax   Total(excl Tax)  ...transaction amount in each major category...
    //
    csvBodyForAccount(accountName: string, transactionType: number, startDate: Date,
                      endDate: Date): Observable<string> {
        return of("body");
    }

    indexForMajorCategory(majorCat: string): number {
        return 1;
    }

    /*
    //
    // Create the report as a csv file in the Documents directory
    //
    func generateReport(accountName: String, transactionType: Int, startDate: Date,
        endDate: Date, callback: (URL?) -> Void) {
        let header = csvHeaderForReportType(transactionType: transactionType)
        let body = csvBodyForAccount(accountName: accountName, transactionType: transactionType, startDate: startDate,
            endDate: endDate)
        let csvStr: String = header + body
        let csvData: Data? = csvStr.data(using: .utf8)
        let df = DateFormatter()
        df.dateFormat = "yyyy_MM_dd"
        let documentsFolderURL: URL? = try? FileManager.default.url(for: .documentDirectory,
            in: .userDomainMask, appropriateFor: nil, create: true)

        // format account name into a format suitable for a file name
        var acctName: String? = UserDefaults.standard.string(forKey: "accountName")
        if let acctNameOrig = acctName {
            let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
            acctName = String(acctNameOrig.filter {okayChars.contains($0) })
        }

        if let documentsFolderURL = documentsFolderURL {
            var fileName: String = ""
            if acctName != nil {
                fileName = acctName! + "_";
            }
            if (transactionType == TransactionType.income.rawValue) {
                fileName += "Income_"
            }
            else {
                fileName += "Expenses_"
            }
            fileName = fileName + df.string(from: startDate) + "_to_" + df.string(from: endDate)
            let fileURL: URL = documentsFolderURL.appendingPathComponent(fileName).appendingPathExtension("csv")
            do {
                try csvData?.write(to: fileURL, options: .atomic)
                callback(fileURL)
            }
            catch {
                consoleLog(message: "ERROR: writing report to documents directory")
                callback(nil)
            }
        }
        else {
            callback(nil)
        }
    }

    // Date   Details  Total(incl Tax)   Tax   Total(excl Tax)     ...transaction amount in each major category...
    func csvBodyForAccount(accountName: String, transactionType: Int, startDate: Date, endDate: Date) -> String {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let nf = NumberFormatter()
        nf.formatterBehavior = .behavior10_4
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        var csvBody: String = ""
        var csvLine: String

        let transactionsFetchRequest: NSFetchRequest<TransactionDataEntity>  = TransactionDataEntity.fetchRequest()
        let transactionsSortDescriptor = NSSortDescriptor(key: "tranDate", ascending: true)
        transactionsFetchRequest.sortDescriptors = [transactionsSortDescriptor]
        transactionsFetchRequest.predicate = NSPredicate(format: "account.accountName==%@ AND
        majorCategory.transactionType==%i AND tranDate>=%@ AND tranDate<=%@", accountName, transactionType,
        startDate as NSDate, endDate as NSDate)

        let appDel = UIApplication.shared.delegate as! AppDelegate
        let context = appDel.persistentContainer!.viewContext
        var tmpStr: String
        do {
            let transactionsArr: Array<TransactionDataEntity> = try context.fetch(transactionsFetchRequest)
            for transaction in transactionsArr {
                // Date
                csvLine = ""
                csvLine = df.string(from: transaction.tranDate! as Date)

                // Details (notes)
                tmpStr = transaction.subCategory!.subCategoryName!
                if (transaction.tranNotes != nil) {
                    tmpStr += transaction.tranNotes!
                }
                tmpStr = tmpStr.replacingOccurrences(of: ",", with: "\\,") // delimit commas
                csvLine += ("," + tmpStr)

                // Total (Inc Tax)
                tmpStr = nf.string(from: (transaction.tranAmount!.floatValue *
                    (transaction.tranPercPersonalUse!.floatValue/100.0)) as NSNumber)!
                tmpStr = tmpStr.replacingOccurrences(of: ",", with : "") // strip commas
                csvLine += ("," + tmpStr)

                // Tax
                tmpStr = nf.string(from: (transaction.tranTax!.floatValue *
                    (transaction.tranPercPersonalUse!.floatValue/100.0)) as NSNumber)!
                tmpStr = tmpStr.replacingOccurrences(of: ",", with : "") // strip commas
                csvLine += ("," + tmpStr)

                // Total (excl Tax)
                let exTaxF: Float = (transaction.tranAmount!.floatValue -
                    transaction.tranTax!.floatValue) * (transaction.tranPercPersonalUse!.floatValue/100.0)
                tmpStr = nf.string(from: exTaxF as NSNumber)!
                tmpStr = tmpStr.replacingOccurrences(of: ",", with : "") // strip commas
                csvLine += ("," + tmpStr)

                // ... the categories ...
                let catIdx = self.indexForMajorCategory(majorCat: transaction.majorCategory!.majorCategoryName!)
                // add leading commas
                var k: Int = 0
                while (k <= catIdx) {
                    csvLine += ","
                    k = k + 1
                }
                csvLine += tmpStr // Total (ex tax) (in relevant column)
                // add trailing commas
                k = catIdx+1
                while (k < self.majorCategoriesArr.count) {
                    csvLine += ","
                    k = k + 1
                }

                csvBody += (csvLine + "\n")
            }
        }
        catch {
            consoleLog(message: "ERROR: fetching transactions")
        }

        return csvBody
    }

    func indexForMajorCategory(majorCat: String) -> Int {
        var idx: Int = 0
        var found: Bool = false
        var i: Int = 0
        while (i<self.majorCategoriesArr.count && !found) {
            if (majorCategoriesArr[i].majorCategoryName == majorCat) {
                consoleLog(message: "Found \(majorCat) at idx \(i)")
                idx = i
                found = true
            }
            i = i + 1
        }

        if (!found) {
            consoleLog(message: "Error: \(majorCat) not found")
        }

        return idx
    }
    */
}
