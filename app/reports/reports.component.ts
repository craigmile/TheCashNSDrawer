import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import * as appSettings from "tns-core-modules/application-settings";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";

import { RouterExtensions } from "nativescript-angular/router";
import { ShareFile } from "nativescript-share-file";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

import * as app from "application";

import { ReportGeneratorService } from "~/reports/report-generator.service";
import { PickerService } from "~/services/picker.service";
import { SharedDataService } from "~/services/shared-data.service";

import { ReportType } from "~/tc-common/enums/report-type.enum";
import { TransactionType } from "~/tc-common/enums/transaction-type.enum";
import { UserDefaultKeys } from "~/tc-common/enums/user-default-keys.enum";
import { IListCell, ListCellType } from "~/tc-common/interfaces/IListCell.interface";

enum ExpandedState {
    notExpanded,
    fromDateExpanded,
    toDateExpanded
}

enum NotExpandedListCell {
    reportType,
    fromDate,
    toDate
}

enum FromDateExpandedListCell {
    reportType,
    fromDate,
    fromDatePicker,
    toDate
}

enum ToDateExpandedListCell {
    reportType,
    fromDate,
    toDate,
    toDatePicker
}

@Component({
    selector: "Reports",
    moduleId: module.id,
    templateUrl: "./reports.component.html"
})
export class ReportsComponent implements OnInit {
    uiListCells: Array<IListCell>;
    expandedState: ExpandedState;
    fromDate: Date;
    toDate: Date;
    reportType: ReportType;

    constructor(
        private pickerService: PickerService,
        private sharedDataService: SharedDataService,
        private reportGeneratorService: ReportGeneratorService,
        private activeRoute: ActivatedRoute,
        private routerExtensions: RouterExtensions
    ) {
        this.expandedState = ExpandedState.notExpanded;
        this.fromDate = new Date();
        this.toDate = new Date();
        // [this.fromDate, this.toDate] = this.financialYearDates();
        console.log("constructor(): setting report type to income");
        this.reportType = ReportType.income;

        this.activeRoute.queryParams.subscribe((params) => {
            console.log("reports - new active route: params: " + params);
            if (params.selectedReportType === undefined) {
                console.log("constructor(): first time in");
            } else {
                console.log("constructor(): returning from a child page with report type: " +
                            this.sharedDataService.reportType);
                // returning to this page from a child page where the user may have changed the shared data
                this.reportType = this.sharedDataService.reportType;
                this.uiListCells = this.presenterCreateViewModelFromModel();
            }
        });

        this.pickerService.newDateSelected.subscribe((newDate: Date) => {
            switch (this.expandedState) {
                case ExpandedState.fromDateExpanded:
                    this.fromDate = newDate;
                    break;
                case ExpandedState.toDateExpanded:
                    this.toDate = newDate;
                    break;
                default:
                    break;
            }
            this.uiListCells = this.presenterCreateViewModelFromModel();
        });
    }

    ngOnInit(): void {
        console.log("ngOnInit()");
        this.uiListCells = this.presenterCreateViewModelFromModel();
    }

    listCellTemplateSelector(item: IListCell, index: number, items: Array<IListCell>) {
        if (item.cellType === ListCellType.datePicker) {
            return "datePickerTemplate";
        }

        return "displayTemplate";
    }

    //
    // Disclosure indicator
    //
    onItemLoading(args: ItemEventData) {
        // note:    args.ios is an instance of UITableViewCell
        if (args.ios && (args.index === 0)) {
            args.ios.accessoryType = 1; // UITableViewCellAccessoryDisclosureIndicator
        }
    }

    // Actions

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onShare(): void {
        console.log("onShare()");
        const acctName = appSettings.getString(UserDefaultKeys.accountName);

        this.reportGeneratorService.generateReport(
            acctName,
            TransactionType.expense,
            this.fromDate, this.toDate).then((filePath) => {
            console.log("onShare(): got a file path: " + filePath);
            const shareFile: ShareFile = new ShareFile();
            shareFile.open(
                {
                    path: filePath,
                    intentTitle: "Open the report with:" // optional Android
                });
        });
    }

    onItemTap(args: ItemEventData): void {
        // this.selected = this.uiListCells[args.index];
        switch (this.expandedState) {
        case ExpandedState.notExpanded:
            switch (args.index) {
                case NotExpandedListCell.reportType:  // show report types available
                    this.routerExtensions.navigate(["/reports/reports-available"], {
                        queryParams: {
                            selectedReportType: this.reportType
                        }
                    });
                    break;
                case NotExpandedListCell.fromDate:
                    this.expandedState = ExpandedState.fromDateExpanded;
                    this.uiListCells = this.presenterCreateViewModelFromModel();
                    break;
                case NotExpandedListCell.toDate:
                    this.expandedState = ExpandedState.toDateExpanded;
                    this.uiListCells = this.presenterCreateViewModelFromModel();
                    break;
            }
            break;
        case ExpandedState.fromDateExpanded:
            switch (args.index) {
                case FromDateExpandedListCell.reportType:  // show report types available
                    this.routerExtensions.navigate(["/reports/reports-available"], {
                        queryParams: {
                            selectedReportType: this.reportType
                        }
                    });
                    break;
                case FromDateExpandedListCell.fromDate:
                    this.expandedState = ExpandedState.notExpanded;
                    this.uiListCells = this.presenterCreateViewModelFromModel();
                    break;
                case FromDateExpandedListCell.toDate:
                    this.expandedState = ExpandedState.toDateExpanded;
                    this.uiListCells = this.presenterCreateViewModelFromModel();
                    break;
            }
            break;
        default: // to date expanded
            switch (args.index) {
                case ToDateExpandedListCell.reportType:  // show report types
                    this.routerExtensions.navigate(["/reports/reports-available"]);
                    break;
                case ToDateExpandedListCell.fromDate:
                    this.expandedState = ExpandedState.fromDateExpanded;
                    this.uiListCells = this.presenterCreateViewModelFromModel();
                    break;
                case ToDateExpandedListCell.toDate:
                    this.expandedState = ExpandedState.notExpanded;
                    this.uiListCells = this.presenterCreateViewModelFromModel();
                    break;
            }
            break;
        }
    }

    private financialYearDates(): [Date, Date] {
        const fromDate = new Date(); // TODO
        const toDate = new Date();

        return [fromDate, toDate];
    }

    /**
     * Presenter
     */

    //
    // Create an array of cells which can be used for the list view in the UI.
    //
    private presenterCreateViewModelFromModel(): Array<IListCell> {
        let tmpCells: Array<IListCell>;
        console.log("presenterCreateViewModelFromModel(): reportType: " + this.reportType);

        switch (this.expandedState) {
            case ExpandedState.notExpanded:
                tmpCells = new Array<IListCell>(
                    {
                        index: NotExpandedListCell.reportType,
                        cellType: ListCellType.hierarchyString,
                        description: this.reportType,
                        value: "",
                        editable: false
                    },
                    {
                        index: NotExpandedListCell.fromDate,
                        cellType: ListCellType.dateString,
                        description: "Start Date",
                        value: this.fromDate.toDateString(),
                        editable: false
                    },
                    {
                        index: NotExpandedListCell.toDate,
                        cellType: ListCellType.dateString,
                        description: "End Date",
                        value: this.toDate.toDateString(),
                        editable: false
                    }
                );
                break;
            case ExpandedState.fromDateExpanded:
                tmpCells = new Array<IListCell>(
                    {
                        index: FromDateExpandedListCell.reportType,
                        cellType: ListCellType.hierarchyString,
                        description: this.reportType,
                        value: "",
                        editable: false
                    },
                    {
                        index: FromDateExpandedListCell.fromDate,
                        cellType: ListCellType.dateString,
                        description: "Start Date",
                        value: this.fromDate.toDateString(),
                        editable: false
                    },
                    {
                        index: FromDateExpandedListCell.fromDatePicker,
                        cellType: ListCellType.datePicker,
                        date: this.fromDate,
                        editable: false
                    },
                    {
                        index: FromDateExpandedListCell.toDate,
                        cellType: ListCellType.dateString,
                        description: "End Date",
                        value: this.toDate.toDateString(),
                        editable: false
                    }
                );
                break;
            case ExpandedState.toDateExpanded:
                tmpCells = new Array<IListCell>(
                    {
                        index: ToDateExpandedListCell.reportType,
                        cellType: ListCellType.hierarchyString,
                        description: this.reportType,
                        value: "",
                        editable: false
                    },
                    {
                        index: ToDateExpandedListCell.fromDate,
                        cellType: ListCellType.dateString,
                        description: "Start Date",
                        value: this.fromDate.toDateString(),
                        editable: false
                    },
                    {
                        index: ToDateExpandedListCell.toDate,
                        cellType: ListCellType.dateString,
                        description: "End Date",
                        value: this.toDate.toDateString(),
                        editable: false
                    },
                    {
                        index: ToDateExpandedListCell.toDatePicker,
                        cellType: ListCellType.datePicker,
                        date: this.toDate,
                        editable: false
                    }
                );
                break;
        }

        return tmpCells;
    }
}
