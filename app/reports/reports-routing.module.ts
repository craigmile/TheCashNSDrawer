import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";

import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ReportsAvailableComponent } from "./reports-available.component";
import { ReportsComponent } from "./reports.component";

const lazyRoutes: Routes = [
    { path: "", component: ReportsComponent },
    { path: "reports-available", component: ReportsAvailableComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(lazyRoutes)],
    exports: [NativeScriptRouterModule]
})
export class ReportsRoutingModule { }
