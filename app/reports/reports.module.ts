import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ReportsAvailableComponent } from "~/reports/reports-available.component";
import { ReportsComponent } from "~/reports/reports.component";

import { ReportsRoutingModule } from "~/reports/reports-routing.module";
import { TcCommonModule } from "~/tc-common/tc-common.module";

import { MajorCategoryService } from "~/services/major-category.service";
import { PickerService } from "~/services/picker.service";
import { ReportGeneratorService } from "./report-generator.service";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ReportsRoutingModule,
        TcCommonModule
    ],
    declarations: [
        ReportsComponent,
        ReportsAvailableComponent
    ],
    providers: [
        PickerService,
        MajorCategoryService,
        ReportGeneratorService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ReportsModule { }
