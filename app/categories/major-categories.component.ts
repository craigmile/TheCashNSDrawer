import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { RouterExtensions } from "nativescript-angular/router";

import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";

import { MajorCategoryService } from "~/services/major-category.service";
import { SharedDataService } from "~/services/shared-data.service";
import { EditMode } from "~/tc-common/enums/edit-mode.enum";
import { IMajorCategory } from "~/tc-common/interfaces/major-category.interface";

@Component({
    selector: "app-major-categories",
    moduleId: module.id,
    templateUrl: "./major-categories.component.html"
})

export class MajorCategoriesComponent implements OnInit {
    uiListCells: Array<IMajorCategory>;
    selectedIdx: number;
    selectedSubCategoryPK: number;
    transactionEditMode: EditMode;
    transactionType: number;

    constructor(
        private majorCategoryService: MajorCategoryService,
        private sharedDataService: SharedDataService,
        private activedRoute: ActivatedRoute,
        private router: Router,
        private routerExtensions: RouterExtensions
    ) {
        console.log("constructor()");
    }

    ngOnInit(): void {
        console.log("ngOnInit()");

        // get the categories from SQL
        this.majorCategoryService.getMajorCategories((majorCategories: Array<IMajorCategory>, errorMsg?: string) => {
            this.uiListCells = majorCategories;

            this.activedRoute.queryParams.subscribe((params) => {
                const majorCategoryPK = JSON.parse(params.majorCategoryPK);
                this.selectedSubCategoryPK = JSON.parse(params.selectedSubCategoryPK);
                this.transactionEditMode = JSON.parse(params.transactionEditMode);
                this.transactionType = JSON.parse(params.transactionType);
                console.log("param subscription: activated route: " + this.activedRoute.toString() +
                "; major category PK of: " + majorCategoryPK +
                "; edit mode: " + this.transactionEditMode +
                "; transaction type: " + this.transactionType);

                if (this.sharedDataService.popToTransactionDetailAndRefresh === true) {
                    console.log("param subscription: go back another");
                    this.routerExtensions.backToPreviousPage(); // navigate back
                } else {
                    // hilight the selected category
                    let i: number = 0;
                    let category: IMajorCategory;
                    for (category of majorCategories) {
                        if (category.PK === majorCategoryPK) {
                            this.selectedIdx = i;
                            break;
                        }
                        i++;
                    }
                    console.log("param subscription: hilight category at index: " + this.selectedIdx);
                }
            });
        });
    }

    //
    // Show sub-categories for the selected category
    //
    onItemTap(args: ItemEventData): void {
        const selectedMajorCategory: IMajorCategory = this.uiListCells[args.index];
        console.log("onItemTap(): index: " + args.index + "; major category PK: " + selectedMajorCategory.PK +
        "; edit mode: " + this.transactionEditMode);
        this.sharedDataService.transactionDetail.majorCategoryPK = selectedMajorCategory.PK;
        this.sharedDataService.transactionDetail.majorCategoryName = selectedMajorCategory.majorCategoryName;

        switch (this.transactionEditMode) {
            case EditMode.create: // in a modal
                this.router.navigate(["./subcategories"], {
                    relativeTo: this.activedRoute,
                    queryParams: {
                        majorCategoryPK: selectedMajorCategory.PK,
                        selectedSubCategoryPK: this.selectedSubCategoryPK,
                        transactionEditMode: this.transactionEditMode,
                        transactionType: this.transactionType
                    }
                });
                break;
            case EditMode.update:
                this.router.navigate(["/categories/subcategories"], {
                    queryParams: {
                        majorCategoryPK: selectedMajorCategory.PK,
                        selectedSubCategoryPK: this.selectedSubCategoryPK,
                        transactionEditMode: this.transactionEditMode,
                        transactionType: this.transactionType
                    }
                });
                break;
            default:
                break;
        }
    }

    //
    // Create a new major category
    //
    onAddPressed() {
        console.log("onAddPressed()");
    }
}
