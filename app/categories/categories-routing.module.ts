import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MajorCategoriesComponent } from "~/categories/major-categories.component";
import { SubCategoriesComponent } from "~/categories/sub-categories/sub-categories.component";

const routes: Routes = [
    {
        path: "", component: MajorCategoriesComponent
    },
    {
        path: "subcategories", component: SubCategoriesComponent
    },
    {
        path: "modal-subcategories",
        loadChildren: "~/categories/categories.module#CategoriesModule", // lazy loaded
        component: SubCategoriesComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})

export class CategoriesRoutingModule { }
