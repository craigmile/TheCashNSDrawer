import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { RouterExtensions } from "nativescript-angular/router";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";

import { SharedDataService } from "~/services/shared-data.service";
import { SubCategoryService } from "~/services/sub-category.service";
import { EditMode } from "~/tc-common/enums/edit-mode.enum";
import { ISubCategory } from "~/tc-common/interfaces/sub-category.interface";

@Component({
    selector: "app-sub-categories",
    moduleId: module.id,
    templateUrl: "./sub-categories.component.html"
})

export class SubCategoriesComponent implements OnInit {
    uiListCells: Array<ISubCategory>;
    selectedIdx?: number;
    transactionEditMode: EditMode;
    transactionType: number;

    constructor(
        private subCategoryService: SubCategoryService,
        private sharedDataService: SharedDataService,
        private activatedRoute: ActivatedRoute,
        private routerExtensions: RouterExtensions
    ) {}

    ngOnInit(): void {
        console.log("ngOnInit()");
        this.activatedRoute.queryParams.subscribe((params) => {
            const majorCategoryPK: number = JSON.parse(params.majorCategoryPK);
            const selectedSubCategoryPK: number = JSON.parse(params.selectedSubCategoryPK);
            this.transactionEditMode = JSON.parse(params.transactionEditMode);
            this.transactionType = JSON.parse(params.transactionType);
            console.log("param subscription: activated route: " + this.activatedRoute +
            "; major category PK = " + majorCategoryPK +
            "; selected sub category: " + selectedSubCategoryPK +
            "; edit mode: " + this.transactionEditMode +
            "; transaction type: " + this.transactionType);
            // get all subcategories for the major category
            this.subCategoryService.getSubCategoriesForMajorCategory(
                majorCategoryPK,
                (subCategories: Array<ISubCategory>, errorMsg?: string) => {
                    this.uiListCells = subCategories;

                    // for (optionally) hilighting the subcategory selected for a major category
                    if (selectedSubCategoryPK !== undefined) {
                        let i: number = 0;
                        let subCategory: ISubCategory;
                        for (subCategory of subCategories) {
                            if (subCategory.PK === selectedSubCategoryPK) {
                                this.selectedIdx = i;
                                break;
                            }
                            i++;
                        }
                    }
                });
        });
    }

    //
    // Actions
    //

    onItemTap(args: ItemEventData): void {
        this.selectedIdx = args.index;
        const selectedSubCategory: ISubCategory = this.uiListCells[args.index];
        this.sharedDataService.transactionDetail.subCategoryPK = selectedSubCategory.PK;
        this.sharedDataService.transactionDetail.subCategoryName = selectedSubCategory.subCategoryName;

        // go back to transaction detail
        // TODO: loosely couple this
        this.sharedDataService.popToTransactionDetailAndRefresh = true;
        this.routerExtensions.backToPreviousPage(); // navigate back
    }

    onAddPressed() {
        console.log("onAddPressed()");
        // present a modal for adding a new sub category
    }
}
