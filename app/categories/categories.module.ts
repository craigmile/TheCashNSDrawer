import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { CategoriesRoutingModule } from "~/categories/categories-routing.module";
import { TcCommonModule } from "~/tc-common/tc-common.module";

import { MajorCategoriesComponent } from "~/categories/major-categories.component";
import { SubCategoriesComponent } from "~/categories/sub-categories/sub-categories.component";

import { MajorCategoryService } from "~/services/major-category.service";
import { SubCategoryService } from "~/services/sub-category.service";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        CategoriesRoutingModule,
        TcCommonModule
    ],
    declarations: [
        MajorCategoriesComponent,
        SubCategoriesComponent
    ],
    providers: [
        MajorCategoryService,
        SubCategoryService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class CategoriesModule { }
