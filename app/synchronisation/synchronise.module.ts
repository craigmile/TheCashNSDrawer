import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SynchroniseRoutingModule } from "~/synchronisation/synchronise-routing.module";
import { SynchroniseComponent } from "~/synchronisation/synchronise.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SynchroniseRoutingModule
    ],
    declarations: [
        SynchroniseComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SynchroniseModule { }
