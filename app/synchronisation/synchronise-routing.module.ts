import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SynchroniseComponent } from "./synchronise.component";

const routes: Routes = [
    { path: "", component: SynchroniseComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SynchroniseRoutingModule { }
