import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { EventData } from "tns-core-modules/data/observable";

import { IDropboxFileDetail } from "./dropbox-handler.object";

const key = "gudr4t1wrg24664";

@Component({
    selector: "Synchronise",
    moduleId: module.id,
    templateUrl: "./synchronise.component.html"
})

export class SynchroniseComponent implements OnInit {
    dropboxFileListDetails: Array<IDropboxFileDetail> = []

    constructor() { //
    }

    ngOnInit(): void { //
    }

    //
    // Actions
    //

    onImportTap(args: EventData) {
        console.log("onImportTap");
    }
    onExportTap(args: EventData) {
        console.log("onExportTap");
    }
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
