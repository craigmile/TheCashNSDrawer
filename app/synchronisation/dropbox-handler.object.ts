/**
 * Handles all interactions between our app and Dropbox (via the Dropbox SDK).
 */
import { Dropbox } from "dropbox";
import { File, Folder, knownFolders } from "tns-core-modules/file-system";

export enum DropboxErrorCode {
    notAuthorised = 1,
    listFilesInFolder = 2,
    downloadFailed = 3
}

export interface IDropboxFileDetail {
    name: string;
    path: string;
    dateModified: Date;
}

export class DropboxHandler {

    CLIENT_ID = "gudr4t1wrg24664";

    //
    // Authorisation
    //
    startAuthorisationFlow() {
        console.log("startAuthorisationFlow()");
        // step 1: get an access token - using the client id
        // get a quick one for testing here: "https://dropbox.github.io/dropbox-api-v2-explorer/#files_list_folder"
        // step 2: use the access token with the dropbox api
        // const dbx = new Dropbox({ accessToken: getAccessTokenFromUrl() });
    }

    // Parse the url and get the access token if it is in the urls hash
    getAccessTokenFromUrl() {
        // return <string>parseQueryString(window.location.hash)["access_token"];
    }

    // If the user was just redirected from authenticating, the urls hash will contain the access token.
    isAuthenticated() {
        // return !!getAccessTokenFromUrl();
    }

    //
    // Files (using access token)
    //

    getListOfFilesAtPath() {
        console.log("getListOfFilesAtPath()");
    }

    downloadFileFromDropbox() {
        console.log("downloadFileFromDropbox()");
    }

    uploadFileToDropbox() {
        // the file to upload is the sql database
        // "TheCash.sqlite"
    }
}
