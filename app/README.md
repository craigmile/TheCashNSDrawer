iOS

`tns run ios --emulator`

Android

start the emulator from Android studio
tools -> AVD Manager

run the command

`tns device`

to get the device id for the emulator

`tns run android --device emulator-5554`