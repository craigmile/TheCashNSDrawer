export enum TransactionDetailListCellType {
    display,
    editable,
    datePicker
}

export interface ITransactionDetailListCell {
    cellType: TransactionDetailListCellType;
    description: string;
    value: string;
}
