/**
 * Data for a single transaction... represented as a table view.
 * Can be used:
 *  + in a modal for adding a new transaction
 *  + in a hierarchy
 */

import { DatePipe, DecimalPipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import * as appSettings from "tns-core-modules/application-settings";
import { device } from "tns-core-modules/platform/platform";
import { DeviceType } from "tns-core-modules/ui/enums/enums";
import { ItemEventData, ListView } from "tns-core-modules/ui/list-view/list-view";

import { ModalDialogParams, ModalDialogService } from "nativescript-angular/modal-dialog";
// services
import { CellService } from "~/services/cell.service";
import { MajorCategoryService } from "~/services/major-category.service";
import { PaymentMethodsService } from "~/services/payment-methods.service";
import { PickerService } from "~/services/picker.service";
import { SharedDataService } from "~/services/shared-data.service";
import { SubCategoryService } from "~/services/sub-category.service";
import { TransactionService } from "~/services/transaction.service";
// common
import { DisplayMode } from "~/tc-common/enums/display-mode.enum";
import { EditMode } from "~/tc-common/enums/edit-mode.enum";
import { TransactionType } from "~/tc-common/enums/transaction-type.enum";
import { UserDefaultKeys } from "~/tc-common/enums/user-default-keys.enum";
// interfaces
import { IListCell, ListCellType } from "~/tc-common/interfaces/IListCell.interface";
import { IMajorCategory } from "~/tc-common/interfaces/major-category.interface";
import { IPaymentMethod } from "~/tc-common/interfaces/payment-method.interface";
import { ISubCategory } from "~/tc-common/interfaces/sub-category.interface";
import { ITransactionDetail } from "~/tc-common/interfaces/transaction-detail.interface";

enum NonExpandedListCell {
    account,
    date,
    amount,
    tax,
    category,
    subCategory,
    payMethod,
    referenceId,
    notes
}

enum ExpandedListCell {
    account,
    date,
    datePicker, // extra row
    amount,
    tax,
    category,
    subCategory,
    payMethod,
    referenceId,
    notes
}

@Component({
    selector: "app-transaction-detail",
    moduleId: module.id,
    providers: [ModalDialogService],
    templateUrl: "./transaction-detail.component.html"
})

export class TransactionDetailComponent implements OnInit {
    uiScreenTitle: string;
    uiListCells: Array<IListCell>;
    uiEditMode: EditMode;
    selectedCell: IListCell;
    isTablet: boolean = device.deviceType === DeviceType.Tablet;
    expanded: boolean = false;
    transactionPK: number;
    transactionType: number;

    constructor(
        private cellService: CellService,
        private transactionService: TransactionService,
        private majorCategoryService: MajorCategoryService,
        private subCategoryService: SubCategoryService,
        private paymentMethodsService: PaymentMethodsService,
        private pickerService: PickerService,
        private sharedDataService: SharedDataService,
        private datePipe: DatePipe,
        private decPipe: DecimalPipe,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private modalParams: ModalDialogParams
    ) {
        this.activeRoute.queryParams.subscribe((params) => {
            this.uiEditMode = JSON.parse(params.transactionEditMode);
            console.log("param subscription: edit mode = " + this.uiEditMode);

            if (this.sharedDataService.popToTransactionDetailAndRefresh === true) {
                // returning here from the right (a child page) where the user may have changed the shared data
                console.log("param subscription: returning from a child page");
                this.sharedDataService.popToTransactionDetailAndRefresh = false;
                this.uiListCells = this.presenterCreateViewModelFromModel();
            } else {
                // coming in from the left or as a modal (to create a new transaction)
                this.transactionType = JSON.parse(params.transactionType);
                switch (this.uiEditMode) {
                    case EditMode.create:
                        this.transactionPK = undefined; // don't know yet
                        break;
                    default:
                        this.transactionPK = JSON.parse(params.transactionPK);
                        break;
                }
                console.log("param subscription: trans edit mode: " + this.uiEditMode +
                "; trans PK: " + this.transactionPK +
                "; trans type: " + this.transactionType);
            }
        });
    }

    ngOnInit() {
        console.log("ngOnInit()");
        // screen title
        this.uiScreenTitle = "";
        if (this.uiEditMode === EditMode.create) {
            this.uiScreenTitle = "Add ";
        }
        if (this.transactionType) {
            switch (this.transactionType) {
                case TransactionType.expense:
                    this.uiScreenTitle += "Expense";
                    break;
                case TransactionType.income:
                    this.uiScreenTitle += "Income";
                    break;
                default:
                    break;
            }
        }

        if (this.uiEditMode === EditMode.create) {
            this.createAndDisplayNewTransaction();
        } else {
            this.fetchAndDisplayTransactionForPK(this.transactionPK);
        }

        // subscribe to user selections - date
        this.pickerService.newDateSelected.subscribe((newDate: Date) => {
            console.log("new date selected: " + newDate);
            this.sharedDataService.transactionDetail.tranDate = newDate;
            this.uiListCells = this.presenterCreateViewModelFromModel();
        });

        // subscribe to user modification of a value (right hand side) in an editable cell
        this.cellService.editableCellValueChange.subscribe((listCell: IListCell) => {
            console.log("editableCellValueChange with index: " + listCell.index +
            "; description: " + listCell.description + "; value: " + listCell.value);
            // populate shared data with changed UI value
            if (this.expanded) {
                switch (listCell.index) {
                    case ExpandedListCell.amount:
                        this.sharedDataService.transactionDetail.amount = parseFloat(listCell.value);
                        break;
                    case ExpandedListCell.tax:
                        this.sharedDataService.transactionDetail.tax = parseFloat(listCell.value);
                        break;
                    case ExpandedListCell.referenceId:
                        this.sharedDataService.transactionDetail.referenceID = listCell.value;
                        break;
                    case ExpandedListCell.notes:
                        this.sharedDataService.transactionDetail.notes = listCell.value;
                        break;
                    default:
                        break;
                }
            } else {
                switch (listCell.index) {
                    case NonExpandedListCell.amount:
                        this.sharedDataService.transactionDetail.amount = parseFloat(listCell.value);
                        break;
                    case NonExpandedListCell.tax:
                        this.sharedDataService.transactionDetail.tax = parseFloat(listCell.value);
                        break;
                    case NonExpandedListCell.referenceId:
                        this.sharedDataService.transactionDetail.referenceID = listCell.value;
                        break;
                    case NonExpandedListCell.notes:
                        this.sharedDataService.transactionDetail.notes = listCell.value;
                        break;
                    default:
                        break;
                }
            }
        });
    }

    // type of the list cell
    listCellTemplateSelector = (item: IListCell, index: number, items: any) => {
        if (item.cellType === ListCellType.datePicker) {
            return "datePickerTemplate";
        } else
        if (item.editable) {
            return "editableTemplate";
        } else {
            return "displayTemplate";
        }
    }

    //
    // Validation
    //

    validateUserEntered(): boolean {
        console.log("validateUserEntered()");

        return true;
    }

    //
    // Disclosure indicator
    //
    onItemLoading(args: ItemEventData): void {
        // note: args.ios is an instance of UITableViewCell
        if (this.uiEditMode !== EditMode.read) {
            if (args.ios) {
                if (this.expanded) {
                    switch (args.index) {
                        case ExpandedListCell.account:
                        case ExpandedListCell.category:
                        case ExpandedListCell.subCategory:
                        case ExpandedListCell.payMethod:
                            args.ios.accessoryType = 1; // UITableViewCellAccessoryDisclosureIndicator
                            break;
                        default:
                            break;
                    }
                } else {
                    switch (args.index) {
                        case NonExpandedListCell.account:
                        case NonExpandedListCell.category:
                        case NonExpandedListCell.subCategory:
                        case NonExpandedListCell.payMethod:
                            args.ios.accessoryType = 1; // UITableViewCellAccessoryDisclosureIndicator
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    //
    // Actions
    //

    onEditPressed(): void {
        console.log("onEditPressed(): current edit mode: " + this.uiEditMode);
        switch (this.uiEditMode) {
            case EditMode.read:
                this.uiEditMode = EditMode.update;
                console.log("onEditPressed(): new edit mode: " + this.uiEditMode);
                this.uiListCells = this.presenterCreateViewModelFromModel(); // editable cells
                break;
            case EditMode.update:
                this.uiEditMode = EditMode.read;
                console.log("onEditPressed(): new edit mode: " + this.uiEditMode);
                this.uiListCells = this.presenterCreateViewModelFromModel(); // display cells
                break;
            case EditMode.create: // not possible: new transaction (modal)
                break;
            case EditMode.delete: // not possible
                break;
        }
    }

    onSavePressed(): void {
        console.log("onSavePressed(): current edit mode: " + this.uiEditMode);
        // const valid: boolean = this.validateUserEntered();
        // save to the database

        switch (this.uiEditMode) {
            case EditMode.create:
                this.transactionService.createTransaction(
                    this.sharedDataService.transactionDetail,
                    (result: number, errorMsg?: string) => {
                        if (errorMsg) {
                            console.error("ERROR: could not create the transaction: " + errorMsg);
                        } else {
                            // remove the modal
                            this.modalParams.closeCallback("OK");
                        }
                    });
                break;
            case EditMode.update:
                this.transactionService.updateTransaction(
                    this.sharedDataService.transactionDetail,
                    (result: number, errorMsg?: string) => {
                        if (errorMsg) {
                            console.error("ERROR: could not update the transaction");
                        } else { // updated the database
                            this.uiEditMode = EditMode.read; // back to display mode
                            console.log("onSavePressed(): new edit mode: " + this.uiEditMode);
                            this.presenterCreateViewModelFromModel(); // for disclosure indicators
                        }
                    });
                break;
            default:
                break;
        }
    }

    //
    // for the modal (add transaction) only
    //
    onCancelPressed() {
        console.log("onCancelPressed()");
        this.modalParams.closeCallback("CANCEL");
    }

    onItemTap(args: ItemEventData): void {
        switch (this.uiEditMode) {
            case EditMode.create:
            case EditMode.update:
                this.selectedCell = this.uiListCells[args.index];
                if (!this.isTablet) {
                    if (this.expanded) {
                        switch (args.index) {
                            case ExpandedListCell.account:
                                this.navigateToAccounts();
                                break;
                            case ExpandedListCell.date:
                                this.expanded = !this.expanded;
                                this.uiListCells = this.presenterCreateViewModelFromModel();
                                break;
                            case ExpandedListCell.category:
                                this.navigateToCategories();
                                break;
                            case ExpandedListCell.subCategory:
                                this.navigateToSubCategories();
                                break;
                            case ExpandedListCell.payMethod:
                                this.navigateToPaymentMethods();
                                break;
                            default:
                                break;
                        }
                    } else {
                        switch (args.index) {
                            case NonExpandedListCell.account:
                                this.navigateToAccounts();
                                break;
                            case NonExpandedListCell.date:
                                if (this.uiEditMode === EditMode.create || this.uiEditMode === EditMode.update) {
                                    this.expanded = !this.expanded;
                                    this.uiListCells = this.presenterCreateViewModelFromModel();
                                }
                                break;
                            case NonExpandedListCell.category:
                                this.navigateToCategories();
                                break;
                            case NonExpandedListCell.subCategory:
                                this.navigateToSubCategories();
                                break;
                            case NonExpandedListCell.payMethod:
                                this.navigateToPaymentMethods();
                                break;
                            default:
                                break;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private navigateToAccounts(): void {
        switch (this.uiEditMode) {
            case EditMode.create: // in a modal
                this.router.navigate(["../modal-accounts"], {
                    relativeTo: this.activeRoute,
                    queryParams: {
                        displayMode: DisplayMode.hierarchy,
                        transactionEditMode: this.uiEditMode
                    }
                });
                break;
            case EditMode.update:
                this.router.navigate(["/accounts"], {
                    queryParams: {
                        displayMode: DisplayMode.hierarchy,
                        transactionEditMode: this.uiEditMode
                    }
                });
                break;
            default:
                break;
        }
    }

    private navigateToCategories(): void {
        switch (this.uiEditMode) {
            case EditMode.create: // in a modal
                this.router.navigate(["../modal-categories"], {
                    relativeTo: this.activeRoute,
                    queryParams: {
                        majorCategoryPK: this.sharedDataService.transactionDetail.majorCategoryPK,
                        selectedSubCategoryPK: this.sharedDataService.transactionDetail.subCategoryPK,
                        transactionEditMode: this.uiEditMode,
                        transactionType: this.transactionType
                    }
                });
                break;
            case EditMode.update:
                this.router.navigate(["/categories"], {
                    queryParams: {
                        majorCategoryPK: this.sharedDataService.transactionDetail.majorCategoryPK,
                        selectedSubCategoryPK: this.sharedDataService.transactionDetail.subCategoryPK,
                        transactionEditMode: this.uiEditMode,
                        transactionType: this.transactionType
                    }
                });
                break;
            default:
                break;
        }
    }

    private navigateToSubCategories(): void {
        switch (this.uiEditMode) {
            case EditMode.create: // in a modal
                this.router.navigate(["../modal-categories/modal-subcategories"], {
                    relativeTo: this.activeRoute,
                    queryParams: {
                        majorCategoryPK: this.sharedDataService.transactionDetail.majorCategoryPK,
                        selectedSubCategoryPK: this.sharedDataService.transactionDetail.subCategoryPK,
                        transactionEditMode: this.uiEditMode,
                        transactionType: this.transactionType
                    }
                });
                break;
            case EditMode.update:
                this.router.navigate(["/categories/subcategories"], {
                    queryParams: {
                        majorCategoryPK: this.sharedDataService.transactionDetail.majorCategoryPK,
                        selectedSubCategoryPK: this.sharedDataService.transactionDetail.subCategoryPK,
                        transactionEditMode: this.uiEditMode,
                        transactionType: this.transactionType
                    }
                });
                break;
            default:
                break;
        }
    }

    private navigateToPaymentMethods(): void {
        switch (this.uiEditMode) {
            case EditMode.create: // in a modal
                this.router.navigate(["../modal-payment-methods"], {
                    relativeTo: this.activeRoute,
                    queryParams: {
                        paymentMethodPK: this.sharedDataService.transactionDetail.payMethodPK,
                        transactionEditMode: this.uiEditMode
                    }
                });
                break;
            case EditMode.update:
                this.router.navigate(["/payment-methods"], {
                    queryParams: {
                        paymentMethodPK: this.sharedDataService.transactionDetail.payMethodPK,
                        transactionEditMode: this.uiEditMode
                    }
                });
                break;
            default:
                break;
        }
    }

    private createAndDisplayNewTransaction(): void {
        // major categories for a transaction type
        this.majorCategoryService.getMajorCategoriesForTransactionType(
            this.transactionType,
            (majorCategories: Array<IMajorCategory>, errorMsg?: string) => { // callback for SQLite
            if (errorMsg) {
                console.error("ERROR: could not get major categories for transaction type: " + this.transactionType);
            } else {
                let majorCategory: IMajorCategory;
                let subCategory: ISubCategory;
                let paymentMethod: IPaymentMethod;
                // get the first major category
                majorCategory = majorCategories[0];
                // get the first subcategory for the major category
                this.subCategoryService.getSubCategoriesForMajorCategory(
                    majorCategory.PK,
                    (subCategories: Array<ISubCategory>, errorMsg2?: string) => {
                        if (errorMsg2) {
                            console.error("ERROR: could not get sub categories for major category: " +
                            majorCategory.majorCategoryName);
                        } else {
                            subCategory = subCategories[0];
                            // get the first payment method
                            this.paymentMethodsService.getPaymentMethods((paymentMethods: Array<IPaymentMethod>,
                                                                          errorMsg3?: string) => {
                                if (errorMsg3) {
                                    console.error("ERROR: could not get payment methods");
                                } else {
                                    paymentMethod = paymentMethods[0];

                                    this.sharedDataService.transactionDetail = {
                                        PK: 0, // not used
                                        tranDate: new Date(),
                                        tranType: this.transactionType,
                                        majorCategoryPK: majorCategory.PK,
                                        majorCategoryName: majorCategory.majorCategoryName,
                                        subCategoryPK: subCategory.PK,
                                        subCategoryName: subCategory.subCategoryName,
                                        amount: 0.0,
                                        tax: 0.0,
                                        payMethodPK: paymentMethod.PK,
                                        payMethodName: paymentMethod.methodName,
                                        accountPK: appSettings.getNumber(UserDefaultKeys.accountPrimaryKey),
                                        accountName: appSettings.getString(UserDefaultKeys.accountName),
                                        referenceID: "",
                                        notes: ""
                                    };
                                    this.uiListCells = this.presenterCreateViewModelFromModel();
                                }
                            });
                        }
                    }
                );
            }
        });
    }

    private fetchAndDisplayTransactionForPK(transactionPK: number): void {
        console.log("fetchAndDisplayTransactionForPK(): PK = " + transactionPK);
        this.transactionService.readTransactionDetail(
            transactionPK,
            (transactionDetail: ITransactionDetail, errorMsg?: string) => {
                this.sharedDataService.transactionDetail = {...transactionDetail};
                this.uiListCells = this.presenterCreateViewModelFromModel();
            });
    }

    //
    // Presenter
    //

    private presenterCreateViewModelFromModel(): Array<IListCell> {
        let tmpCells: Array<IListCell>;
        let editableCell: boolean = false;

        switch (this.uiEditMode) {
            case EditMode.create:
            case EditMode.update:
                editableCell = true;
                break;
            case EditMode.read:
            case EditMode.delete:
                break;
        }

        if (this.expanded) {
            this.cellService.setInlineEditableCellIndexes([
                ExpandedListCell.amount,
                ExpandedListCell.tax,
                ExpandedListCell.referenceId,
                ExpandedListCell.notes
            ]);
            tmpCells = new Array<IListCell>(
                {
                    index: ExpandedListCell.account,
                    cellType: ListCellType.hierarchyString,
                    description: "Account",
                    value: this.sharedDataService.transactionDetail.accountName,
                    editable: false
                },
                {
                    index: ExpandedListCell.date,
                    cellType: ListCellType.dateString, // expands to provide a date picker
                    description: "Date",
                    value: this.datePipe.transform(this.sharedDataService.transactionDetail.tranDate, "d MMMM yyyy"),
                    editable: false
                },
                {
                    index: ExpandedListCell.datePicker,
                    cellType: ListCellType.datePicker,
                    date: this.sharedDataService.transactionDetail.tranDate,
                    editable: true
                },
                {
                    index: ExpandedListCell.amount,
                    cellType: ListCellType.monetaryAmount,
                    description: "Amount\n(Inc Tax)",
                    value: this.decPipe.transform(this.sharedDataService.transactionDetail.amount, "1.2-2"),
                    editable: editableCell
                },
                {
                    index: ExpandedListCell.tax,
                    cellType: ListCellType.monetaryAmount,
                    description: "Tax",
                    value: this.decPipe.transform(this.sharedDataService.transactionDetail.tax, "1.2-2"),
                    editable: editableCell
                },
                {
                    index: ExpandedListCell.category,
                    cellType: ListCellType.hierarchyString,
                    description: "Category",
                    PK: this.sharedDataService.transactionDetail.majorCategoryPK,
                    value: this.sharedDataService.transactionDetail.majorCategoryName,
                    editable: false
                },
                {
                    index: ExpandedListCell.subCategory,
                    cellType: ListCellType.hierarchyString,
                    description: "Subcategory",
                    value: this.sharedDataService.transactionDetail.subCategoryName,
                    editable: false
                },
                {
                    index: ExpandedListCell.payMethod,
                    cellType: ListCellType.hierarchyString,
                    description: "Pay\nMethod",
                    value: this.sharedDataService.transactionDetail.payMethodName,
                    editable: false
                },
                {
                    index: ExpandedListCell.referenceId,
                    cellType: ListCellType.descriptionString,
                    description: "Reference ID",
                    value: this.sharedDataService.transactionDetail.referenceID,
                    editable: editableCell
                },
                {
                    index: ExpandedListCell.notes,
                    cellType: ListCellType.descriptionString,
                    description: "Notes",
                    value: this.sharedDataService.transactionDetail.notes,
                    editable: editableCell
                }
            );
        } else {
            this.cellService.setInlineEditableCellIndexes([
                NonExpandedListCell.amount,
                NonExpandedListCell.tax,
                NonExpandedListCell.referenceId,
                NonExpandedListCell.notes
            ]);
            tmpCells = new Array<IListCell>(
                {
                    index: NonExpandedListCell.account,
                    cellType: ListCellType.hierarchyString,
                    description: "Account",
                    value: this.sharedDataService.transactionDetail.accountName,
                    editable: false
                },
                {
                    index: NonExpandedListCell.date,
                    cellType: ListCellType.dateString, // expands to provide a date picker
                    description: "Date",
                    value: this.datePipe.transform(this.sharedDataService.transactionDetail.tranDate, "d MMMM yyyy"),
                    editable: false
                },
                {
                    index: NonExpandedListCell.amount,
                    cellType: ListCellType.monetaryAmount,
                    description: "Amount\n(Inc Tax)",
                    value: this.decPipe.transform(this.sharedDataService.transactionDetail.amount, "1.2-2"),
                    editable: editableCell
                },
                {
                    index: NonExpandedListCell.tax,
                    cellType: ListCellType.monetaryAmount,
                    description: "Tax",
                    value: this.decPipe.transform(this.sharedDataService.transactionDetail.tax, "1.2-2"),
                    editable: editableCell
                },
                {
                    index: NonExpandedListCell.category,
                    cellType: ListCellType.hierarchyString,
                    description: "Category",
                    PK: this.sharedDataService.transactionDetail.majorCategoryPK,
                    value: this.sharedDataService.transactionDetail.majorCategoryName,
                    editable: false
                },
                {
                    index: NonExpandedListCell.subCategory,
                    cellType: ListCellType.hierarchyString,
                    description: "Subcategory",
                    value: this.sharedDataService.transactionDetail.subCategoryName,
                    editable: false
                },
                {
                    index: NonExpandedListCell.payMethod,
                    cellType: ListCellType.hierarchyString,
                    description: "Pay\nMethod",
                    value: this.sharedDataService.transactionDetail.payMethodName,
                    editable: false
                },
                {
                    index: NonExpandedListCell.referenceId,
                    cellType: ListCellType.descriptionString,
                    description: "Reference ID",
                    value: this.sharedDataService.transactionDetail.referenceID,
                    editable: editableCell
                },
                {
                    index: NonExpandedListCell.notes,
                    cellType: ListCellType.descriptionString,
                    description: "Notes",
                    value: this.sharedDataService.transactionDetail.notes,
                    editable: editableCell
                }
            );
        }

        return tmpCells;
    }
}
