import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { TransactionDetailComponent } from "~/transactions/transaction-detail/transaction-detail.component";

const routes: Routes = [
    { path: "", component: TransactionDetailComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})

export class TransactionDetailRoutingModule { }
