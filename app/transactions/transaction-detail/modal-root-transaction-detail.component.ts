/**
 * Displayed modally
 * eg: from TransactionsComponent
 */
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { EditMode } from "~/tc-common/enums/edit-mode.enum";

@Component({
  moduleId: module.id,
  templateUrl: "./modal-root-transaction-detail.component.html"
})
export class ModalRootTransactionDetailComponent implements OnInit {
  constructor(
    private routerExtensions: RouterExtensions,
    private activeRoute: ActivatedRoute,
    private modalParams: ModalDialogParams
  ) {}

  ngOnInit(): void {
    console.log("Trans type is: " + this.modalParams.context.transactionType);
    this.routerExtensions.navigate(["modal-transaction-detail"],
      {
      relativeTo: this.activeRoute,
      queryParams: {
        transactionPK: undefined, // not used yet
        transactionType: this.modalParams.context.transactionType,
        transactionEditMode: EditMode.create
      }
    });
  }
}
