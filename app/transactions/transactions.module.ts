import { CurrencyPipe, DatePipe, DecimalPipe } from "@angular/common";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ModalDialogParams, ModalDialogService } from "nativescript-angular/modal-dialog";

import { TcCommonModule } from "../tc-common/tc-common.module";
import { TransactionsRoutingModule } from "./transactions-routing.module";

import { AccountService } from "~/services/account.service";
import { CellService } from "~/services/cell.service";
import { MajorCategoryService } from "~/services/major-category.service";
import { PaymentMethodsService } from "~/services/payment-methods.service";
import { PickerService } from "~/services/picker.service";
import { SubCategoryService } from "~/services/sub-category.service";
import { TransactionService } from "~/services/transaction.service";
import { TransactionSummaryCellService } from "~/transactions/transaction-list/cells/transaction-summary-cell.service";

import { ModalRootTransactionDetailComponent } from "./transaction-detail/modal-root-transaction-detail.component";
import { TransactionDetailComponent } from "./transaction-detail/transaction-detail.component";
import { TransactionHeaderCellComponent } from "./transaction-list/cells/transaction-header-cell.component";
import { TransactionSummaryCellComponent } from "./transaction-list/cells/transaction-summary-cell.component";
import { TransactionsComponent } from "./transaction-list/transactions.component";

export function defaultModalParamsFactory() {
    return new ModalDialogParams({}, null);
}
@NgModule({
    imports: [
        NativeScriptCommonModule,
        TransactionsRoutingModule,
        TcCommonModule
    ],
    declarations: [
        ModalRootTransactionDetailComponent,
        TransactionsComponent,
        TransactionHeaderCellComponent,
        TransactionSummaryCellComponent,
        TransactionDetailComponent
    ],
    providers: [
        CurrencyPipe,
        DatePipe,
        DecimalPipe,
        PickerService,
        AccountService,
        TransactionService,
        MajorCategoryService,
        SubCategoryService,
        PaymentMethodsService,
        ModalDialogService,
        CellService,
        TransactionSummaryCellService,
        { provide: ModalDialogParams, useFactory: defaultModalParamsFactory }
    ],
    entryComponents: [
        ModalRootTransactionDetailComponent,
        TransactionDetailComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class TransactionsModule { }
