export interface ISectionHeader {
    title: string;
    numRows: number;
}
