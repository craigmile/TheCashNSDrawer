/**
 * Used in the transactions list
 */
export enum TransactionsListCellType {
    sectionHeader,
    transactionSummary
}
