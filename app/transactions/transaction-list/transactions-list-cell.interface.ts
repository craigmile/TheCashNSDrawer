import { TransactionsListCellType } from "./TransactionsListCellType";

export interface ITransactionsListCell {
    cellType: TransactionsListCellType;
    // section header
    sectionHeader?: string;
    // transaction summary
    PK?: number;
    tranDate?: Date;
    majorCategoryName?: string;
    subCategoryName?: string;
    amount?: number;
    type?: number;
}
