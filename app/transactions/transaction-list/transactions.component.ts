//
// A list of transactions for a chosen account.
//
import { Component, ElementRef, OnInit, ViewChild, ViewContainerRef } from "@angular/core";

import { ModalDialogOptions, ModalDialogService } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

import * as application from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";
import * as app from "tns-core-modules/application/application";
import { EventData } from "tns-core-modules/data/observable";
import { device, isAndroid, isIOS } from "tns-core-modules/platform/platform";
import * as dialogs from "tns-core-modules/ui/dialogs/dialogs";
import { DeviceType } from "tns-core-modules/ui/enums/enums";
import { ItemEventData, ListView } from "tns-core-modules/ui/list-view/list-view";

import { AccountService } from "~/services/account.service";
import { TransactionService } from "~/services/transaction.service";

import { IAccount } from "~/accounts/account.interface";
import { EditMode } from "~/tc-common/enums/edit-mode.enum";
import { TransactionType } from "~/tc-common/enums/transaction-type.enum";
import { UserDefaultKeys } from "~/tc-common/enums/user-default-keys.enum";
import { platformSpecificListDataSource } from "~/tc-common/list-data-source-index";
// tslint:disable-next-line:max-line-length
import { ModalRootTransactionDetailComponent } from "~/transactions/transaction-detail/modal-root-transaction-detail.component";
import { TransactionSummaryCellService } from "~/transactions/transaction-list/cells/transaction-summary-cell.service";
import { ITransactionsListCell } from "~/transactions/transaction-list/transactions-list-cell.interface";
import { TransactionsListCellType } from "~/transactions/transaction-list/TransactionsListCellType";
import { ISectionHeader } from "./section-header.interface";
import { ITransaction } from "./transaction.interface";

@Component({
  selector: "app-transactions",
  moduleId: module.id,
  templateUrl: "./transactions.component.html",
  providers: [ModalDialogService]
})
export class TransactionsComponent implements OnInit {
  @ViewChild("transListView")transListViewRef: ElementRef;

  uiListCells: Array<ITransactionsListCell>;
  uiTitle: string;

  selectedCell: ITransactionsListCell;
  isTablet: boolean = device.deviceType === DeviceType.Tablet;
  listViewModified; boolean = false;
  isIOS: boolean;
  isAndroid: boolean;

  constructor(
    private accountService: AccountService,
    private transactionService: TransactionService,
    private modalService: ModalDialogService,
    private vcRef: ViewContainerRef,
    private routerExtensions: RouterExtensions,
    private transactionSummaryCellService: TransactionSummaryCellService
  ) {
    console.log("constructor()");
    this.isIOS = isIOS;
    this.isAndroid = isAndroid;
  }

  ngOnInit(): void {
    console.log("ngOnInit()");
    this.isTablet = device.deviceType === DeviceType.Tablet;

    // using this approach to get correct index in a sectioned list view
    this.transactionSummaryCellService.transactionSummaryCellSelected.subscribe(
      (transaction: ITransactionsListCell) => {
      console.log("Selected transaction cell with PK: " + transaction.PK);
      // for phone users we navigate to another screen to show the detail view
      if (!this.isTablet) {
          this.routerExtensions.navigate(["/transactions/transaction-detail"], {
            queryParams: {
              transactionPK: transaction.PK,
              transactionType: transaction.type,
              transactionEditMode: EditMode.read
            }
          });
        }
    });

    this.transactionSummaryCellService.transactionSummaryCellSwipeToDeleteAndroid.subscribe((transPK: number) => {
      console.log("Android swipe to delete: " + transPK);
      this.deleteTransactionForPKAndRefreshUI(transPK);
    });

    this.accountService.getAccounts((accounts: Array<IAccount>, errorMsg?: string) => {
      if (accounts.length > 0) {
        console.log("TransactionsComponent: ngOnInit(): There are " + accounts.length + " accounts in the database");
        const selectedAccountName = appSettings.getString(UserDefaultKeys.accountName);
        const selectedAccountPK = appSettings.getNumber(UserDefaultKeys.accountPrimaryKey);
        console.log("TransactionsComponent: ngOnInit(): selected account name: " + selectedAccountName +
          "; key: " + selectedAccountPK);
        if ((selectedAccountName == null) || (selectedAccountPK == null)) {
          // there is no selected account so use the first account available
          const firstAccount: IAccount = accounts[0];
          appSettings.setNumber(UserDefaultKeys.accountPrimaryKey, firstAccount.PK);
          appSettings.setString(UserDefaultKeys.accountName, firstAccount.accountName);
        }

        this.uiTitle = appSettings.getString(UserDefaultKeys.accountName);
      } else {
        // TODO: handle this scenario better
        dialogs.alert("ERROR: There are no accounts in the database");
      }
    });
  }

  //
  // Whether to use a section header cell or a summary cell in the UI
  //
  listCellTemplateSelector = (item: ITransactionsListCell) => {
    if (item.cellType === TransactionsListCellType.sectionHeader) {
      return "header";
    }

    return "summary";
  }

  //
  // Disclosure indicator
  //
  onItemLoading(args: ItemEventData) {
    // note: args.ios is an instance of UITableViewCell
    // note: timing of this is important
    if (args.ios) {
      const loadingCell = this.uiListCells[args.index];
      if (
        loadingCell.cellType === TransactionsListCellType.transactionSummary
      ) {
        args.ios.accessoryType = 1; // UITableViewCellAccessoryDisclosureIndicator
      }
    }
  }

  //
  // Used to get the correct event timing so that we have access to the iOS (and later Android)
  // native objects for section headers (eg: overriding data source methods)
  //
  onListViewLoaded(args: EventData) {
    console.log("onListViewLoaded()");
    this.refreshTransactions();
  }

  //
  // Actions
  //

  //
  // Display an action sheet to choose Income / Expense
  // Display a modal to accept the transaction details
  //
  onAddPressed() {
    console.log("TransactionsComponent: onAddPressed()");

    dialogs
      .action({
        message: null,
        cancelButtonText: "Cancel",
        actions: ["Income", "Expense"]
      })
      .then((transactionType) => {
        switch (transactionType) {
          case "Expense":
            this.showModalForAddTransaction(TransactionType.expense);
            break;
          case "Income":
            this.showModalForAddTransaction(TransactionType.income);
            break;
          default:
            break;
        }
      });
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  private showModalForAddTransaction(transTypeNum: number) {
    this.createModalViewForAddTransaction(transTypeNum)
    .then((result) => {
      console.log("TransactionsComponent: onAddPressed(): got a result from the modal: " + result);
      if (this.isAndroid) { // quirk: Android does not trigger the onLoaded event for the ListView
        this.refreshTransactions();
      }
    })
    .catch((error) => {
      console.error("ERROR: onAddPressed(): creating modal: " + error);
    });
  }

  private createModalViewForAddTransaction(transactionType: number): Promise<any> {
    console.log("TransactionsComponent: createModalViewForAddTransaction(): transaction type: " + transactionType);
    const options: ModalDialogOptions = {
      viewContainerRef: this.vcRef,
      context: {
        // tslint:disable-next-line:object-literal-shorthand
        transactionType: transactionType
      },
      fullscreen: true
    };

    return this.modalService
      .showModal(ModalRootTransactionDetailComponent, options)
      .then((result: string) => {
        console.log("TransactionsComponent: Result from modal transaction detail: " + result);
      });
  }

  private refreshTransactions() {
    this.transactionService.readTransactions(
      appSettings.getNumber(UserDefaultKeys.accountPrimaryKey),
      (trans: Array<ITransaction>) => {
        this.uiListCells = this.presenterCreateViewModelFromModel(trans);
        console.log(`TransactionsComponent: ngOnInit(): got ` + trans.length +
          ` transactions and  ` + this.uiListCells.length + ` cells`);
      }
    );
  }

  //
  // For swipe to delete and section headers (sticky)
  //
  private modifyListView(headersArrayiOS: Array<ISectionHeader>): void {
    // console.log("modifyListView()");
    const transListView: ListView = this.transListViewRef.nativeElement;
    if (transListView.ios) { // UITableView
      this.modifyiOSDataSource(transListView, headersArrayiOS); // swipe to delete and section headers
    }
  }

  private modifyiOSDataSource(transListView: ListView, headersArrayiOS: Array<ISectionHeader>): void {
    // console.log("modifyiOSDataSource()");
    const weakRefToListView: WeakRef<ListView> = new WeakRef(transListView);
    const newDataSource = platformSpecificListDataSource.initWithOwner(weakRefToListView, headersArrayiOS);
    transListView.ios.dataSource = newDataSource;

    newDataSource.rowDeletedEventStream.subscribe(
      (idx: number) => {
        this.deleteTransactionAtCellIndexAndRefreshUI(idx);
      }, // next
      (err) => console.error("Error: " + err),
      () => console.log("Complete") // complete
    );

    this.listViewModified = true;
  }

  private deleteTransactionAtCellIndexAndRefreshUI(idx: number): void {
    console.log("deleteTransactionAtCellIndexAndRefreshUI(): cell index: " + idx);
    this.deleteTransactionForPKAndRefreshUI(this.uiListCells[idx].PK);
  }

  private deleteTransactionForPKAndRefreshUI(pk: number): void {
    this.transactionService.deleteTransaction(pk, (result: any, errorMsg?: string) => {
      console.log("deleteTransactionForPKAndRefreshUI(): returned from delete sql: result: " +
        result + "; errorMsg: " + errorMsg);
      if (errorMsg) {
        console.error("deleteTransactionForPKAndRefreshUI: ERROR: " + errorMsg);
      } else {
        this.refreshTransactions();
      }
    });
  }

  /**
   * Presenter
   */

  //
  // Add section headers - year and month
  //
  private presenterCreateViewModelFromModel(transactions: Array<ITransaction>): Array<ITransactionsListCell> {
    console.log("TransactionsComponent: presenterCreateViewModelFromModel(): " +
                transactions.length + " transactions");
    let prevYr: number = 1800;
    let prevMth: number = 1;
    let yr: number;
    let mth: number;
    const mthArr: Array<string> = [
      "January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    const transactionsUI = new Array<ITransactionsListCell>();
    let numRowsInSection: number = 0;
    let sectionIdx: number = -1;

    const headersArrayiOS: Array<ISectionHeader> = new Array<ISectionHeader>();

    for (const transaction of transactions) {
      yr = transaction.tranDate.getFullYear();
      mth = transaction.tranDate.getMonth();

      // section header
      if (yr !== prevYr || mth !== prevMth) {
        // iOS: section header is native
        if (application.ios) {
          sectionIdx++;
          if (sectionIdx > 0) {
            // now we know the number of rows in the previous section
            headersArrayiOS[sectionIdx - 1].numRows = numRowsInSection;
          }
          numRowsInSection = 0;
          const sectionHeader: ISectionHeader = {
            title: String(yr) + " " + mthArr[mth],
            numRows: numRowsInSection
          };
          headersArrayiOS.push(sectionHeader);
        } else
        // Android: section header implemented using an Angular template (not native)
        if (application.android) {
          transactionsUI.push({
            cellType: TransactionsListCellType.sectionHeader,
            sectionHeader: String(yr) + " " + mthArr[mth],
            PK: 99,
            tranDate: null,
            majorCategoryName: null,
            subCategoryName: null,
            amount: null,
            type: null
          });
        }

        prevYr = yr;
        prevMth = mth;
      }

      // transaction summary
      transactionsUI.push({
        cellType: TransactionsListCellType.transactionSummary,
        sectionHeader: null,
        PK: transaction.PK,
        tranDate: transaction.tranDate,
        majorCategoryName: transaction.majorCategoryName,
        subCategoryName: transaction.subCategoryName,
        amount: transaction.amount,
        type: transaction.type
      });

      numRowsInSection++;
    }

    // update the final section header (iOS only)
    if (application.ios) {
      // now we know the number of rows in the previous section
      headersArrayiOS[sectionIdx].numRows = numRowsInSection;

      // if (!this.listViewModified) {
      this.modifyListView(headersArrayiOS);
      // }
    }

    return transactionsUI;
  }
}
