export interface ITransaction {
    PK: number;
    tranDate: Date;
    majorCategoryName: string;
    subCategoryName: string;
    amount: number;
    type: number;
}
