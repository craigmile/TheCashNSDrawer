import { Component, Input } from "@angular/core";

import { ITransactionsListCell } from "~/transactions/transaction-list/transactions-list-cell.interface";

@Component({
    selector: "app-transaction-header-cell",
    moduleId: module.id,
    templateUrl: "./transaction-header-cell.component.html"
})

export class TransactionHeaderCellComponent {
    @Input() headerCell: ITransactionsListCell;
}
