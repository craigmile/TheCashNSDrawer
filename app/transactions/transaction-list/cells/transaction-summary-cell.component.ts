import { Component, ElementRef, Input, OnChanges, ViewChild } from "@angular/core";

import { isAndroid } from "tns-core-modules/platform";
import { SwipeDirection, SwipeGestureEventData } from "tns-core-modules/ui/gestures/gestures";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout/stack-layout";
import { AnimationCurve } from "ui/enums";

import { TransactionType } from "~/tc-common/enums/transaction-type.enum";
import { TransactionSummaryCellService } from "~/transactions/transaction-list/cells/transaction-summary-cell.service";
import { ITransactionsListCell } from "~/transactions/transaction-list/transactions-list-cell.interface";

@Component ({
    selector: "app-transaction-summary-cell",
    moduleId: module.id,
    templateUrl: "./transaction-summary-cell.component.html"
})

export class TransactionSummaryCellComponent implements OnChanges {
    @ViewChild("topLayer")stackLayoutRef: ElementRef;

    @Input() summaryCell: ITransactionsListCell; // get data into the cell

    isExpense: boolean = false;
    isAndroid: boolean;

    constructor(
        private transactionSummaryCellService: TransactionSummaryCellService
    ) {
        this.isAndroid = isAndroid;
    }

    ngOnChanges(): void {
        this.isExpense = false;
        if (this.summaryCell.type === TransactionType.expense) {
            this.isExpense = true;
        }
    }

    //
    // Actions
    //

    // cell tap
    onTap(selectedCell: ITransactionsListCell): void {
        console.log("onTap(): " + selectedCell.PK);
        this.transactionSummaryCellService.transactionSummaryCellSelected.emit(selectedCell);
    }

    // handle on Android only
    onTapDelete(): void {
        console.log("onTapDelete()");
        if (isAndroid) {
            const topStack: StackLayout = this.stackLayoutRef.nativeElement;
            this.transactionSummaryCellService.transactionSummaryCellSwipeToDeleteAndroid.emit(
                this.summaryCell.PK
            );
            topStack.animate({
                duration: 0,
                curve: AnimationCurve.easeInOut,
                translate: { x: 0, y: 0 }
            });
        }
    }

    // handle on Android only
    onSwipe(args: SwipeGestureEventData) {
        console.log("onSwipe(): direction: " + args.direction);
        if (isAndroid) {
            console.log("onSwipe(): Android: object " + args.object + "; view: " + args.view +
                        "; event: " + args.eventName);
            const topStack: StackLayout = this.stackLayoutRef.nativeElement;
            switch (args.direction) {
                case SwipeDirection.left:
                    console.log("left"); // delete
                    topStack.animate({
                        duration: 400,
                        curve: AnimationCurve.easeInOut,
                        translate: { x: -100, y: 0 }
                    }).then(() => {
                        console.log("swipe left animation done");
                    });
                    break;
                case SwipeDirection.right:
                    console.log("right");
                    topStack.animate({
                        duration: 400,
                        curve: AnimationCurve.easeInOut,
                        translate: { x: 0, y: 0 }
                    }).then(() => {
                        console.log("swipe right animation done");
                    });
                    break;
                default:
                    break;
            }
        }
    }
}
