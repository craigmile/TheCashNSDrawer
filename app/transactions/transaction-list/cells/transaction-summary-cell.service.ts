import { EventEmitter, Injectable, Output } from "@angular/core";
import { ITransactionsListCell } from "~/transactions/transaction-list/transactions-list-cell.interface";

@Injectable()
export class TransactionSummaryCellService {

    @Output() transactionSummaryCellSelected = new EventEmitter<ITransactionsListCell>();
    @Output() transactionSummaryCellSwipeToDeleteAndroid = new EventEmitter<number>();
}
