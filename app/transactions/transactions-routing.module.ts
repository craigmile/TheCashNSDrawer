import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { TransactionDetailComponent } from "~/transactions/transaction-detail/transaction-detail.component";
import { TransactionsComponent } from "~/transactions/transaction-list/transactions.component";

const routes: Routes = [
    {
        path: "", component: TransactionsComponent,
        children: [ // for the modal (add transaction)
            {
                path: "modal-transaction-detail",
                component: TransactionDetailComponent
            },
            {
                path: "modal-accounts",
                loadChildren: "~/accounts/accounts.module#AccountsModule" // lazy loaded
            },
            {
                path: "modal-categories",
                loadChildren: "~/categories/categories.module#CategoriesModule" // lazy loaded
            },
            {
                path: "modal-payment-methods",
                loadChildren: "~/payment-methods/payment-methods.module#PaymentMethodsModule" // lazy loaded
            }
        ]
    },
    {
        path: "transaction-detail",
        component: TransactionDetailComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})

export class TransactionsRoutingModule { }
