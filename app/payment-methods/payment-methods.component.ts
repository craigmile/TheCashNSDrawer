import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import * as app from "application";
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";

import { PaymentMethodsService } from "~/services/payment-methods.service";
import { SharedDataService } from "~/services/shared-data.service";
import { DisplayMode } from "~/tc-common/enums/display-mode.enum";
import { EditMode } from "~/tc-common/enums/edit-mode.enum";
import { IPaymentMethod } from "../tc-common/interfaces/payment-method.interface";

@Component({
    selector: "payment-methods",
    moduleId: module.id,
    templateUrl: "./payment-methods.component.html"
})

export class PaymentMethodsComponent implements OnInit {
    uiListCells: Array<IPaymentMethod>;
    selectedPaymentMethodPK: number;
    selectedIdx: number;

    constructor(
        private route: ActivatedRoute,
        private paymentMethodsService: PaymentMethodsService,
        private routerExtensions: RouterExtensions,
        private sharedDataService: SharedDataService
    ) {
        console.log("constructor()");
        this.route.queryParams.subscribe((params) => {
            this.selectedPaymentMethodPK = JSON.parse(params.paymentMethodPK);
            console.log("constructor(): selectedPaymentMethodPK: " + this.selectedPaymentMethodPK);
        });
    }

    ngOnInit(): void {
        this.paymentMethodsService.getPaymentMethods((paymentMethods: Array<IPaymentMethod>, errorMsg?: string) => {
            if (errorMsg) {
                console.error("ERROR: could not get payment methods: " + errorMsg);
            } else {
                // is there a payment method already selected?
                let i = 0;
                let paymentMethod: IPaymentMethod;
                for (paymentMethod of paymentMethods) {
                    if (paymentMethod.PK === this.selectedPaymentMethodPK) {
                        this.selectedIdx = i;
                        break;
                    }
                    i++;
                }
                console.log("ngOnInit(): selected index = " + this.selectedIdx);

                this.uiListCells = paymentMethods;
            }
        });
    }

    //
    // Actions
    //

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args: ItemEventData): void {
        this.selectedIdx = args.index;
        // navigate back with the selected payment method
        this.sharedDataService.transactionDetail.payMethodPK = this.uiListCells[args.index].PK;
        this.sharedDataService.transactionDetail.payMethodName = this.uiListCells[args.index].methodName;
        this.sharedDataService.popToTransactionDetailAndRefresh = true;
        console.log("onItemTap(): payment method: " + this.sharedDataService.transactionDetail.payMethodName);
        this.routerExtensions.back();
    }

    onAddPressed() {
        console.log("onAddPressed()");
        // present a modal for adding a new payment method
    }
}
