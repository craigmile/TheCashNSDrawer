import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { PaymentMethodsRoutingModule } from "~/payment-methods/payment-methods-routing.module";
import { PaymentMethodsComponent } from "~/payment-methods/payment-methods.component";
import { PaymentMethodsService } from "~/services/payment-methods.service";
import { TcCommonModule } from "~/tc-common/tc-common.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PaymentMethodsRoutingModule,
        TcCommonModule
    ],
    declarations: [
        PaymentMethodsComponent
    ],
    providers: [
        PaymentMethodsService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class PaymentMethodsModule { }
