import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";

import { NativeScriptRouterModule } from "nativescript-angular/router";

import { PaymentMethodsComponent } from "./payment-methods.component";

const routes: Routes = [
    { path: "", component: PaymentMethodsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PaymentMethodsRoutingModule { }
