import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

export const COMPONENTS = [
];

const routes: Routes = [
    {
        path: "",
        redirectTo: "/transactions",
        pathMatch: "full"
    },
    {
        path: "transactions",
        loadChildren: "~/transactions/transactions.module#TransactionsModule" // lazy loaded
    },
    {
        path: "categories",
        loadChildren: "~/categories/categories.module#CategoriesModule" // lazy loaded
    },
    {
        path: "payment-methods",
        loadChildren: "~/payment-methods/payment-methods.module#PaymentMethodsModule" // lazy loaded
    },
    {
        path: "accounts",
        loadChildren: "~/accounts/accounts.module#AccountsModule" // lazy loaded
    },
    {
        path: "synchronise",
        loadChildren: "~/synchronisation/synchronise.module#SynchroniseModule" // lazy loaded
    },
    {
        path: "reports",
        loadChildren: "~/reports/reports.module#ReportsModule" // lazy loaded
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})

export class AppRoutingModule { }
